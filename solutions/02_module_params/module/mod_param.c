/**
 * @file mod_param.c
 * @brief Module parameters.
 * @details This module receives parameters at the time it is being loaded.
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 * @author Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *}
 */


/**
 * @page p2 Lab 02: Module parameters
 *
 * @tableofcontents
 *
 * In this lab you will learn how to add support to your module to receive
 * parameters at the time it is being loaded.
 */

/**
 * @page p2
 * @section p2s1 Step 1: Add support for module parameters
 *
 * To enable passing arguments to this kernel module we need to include the
 * header files which enable the support for module parameters.
 *
 * Include the header files here:
 *
 * @code
 *	#include <linux/module.h>
 *	#include <linux/moduleparam.h>
 *	#include <linux/stat.h>
 * @endcode
 */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>

/**
 * @page p2
 * @section p2s2 Step 2: Declare the module parameters
 *
 * Declare some global variables to be used as module parameters:
 *
 * @code
 * static short short_param = -10;
 * static int int_param = -10000;
 * static long long_param = -1000000;
 * static unsigned short ushort_param = 10;
 * static unsigned int uint_param = 10000;
 * static unsigned long ulong_param = 1000000;
 * static char *charp_param = "This is a charp param";
 * static int int_array[5] = {0, 1, 2, 3, 4};
 * static unsigned int arr_num = 5;
 * @endcode
 */
static short short_param = -10;
static int int_param = -10000;
static long long_param = -1000000;
static unsigned short ushort_param = 10;
static unsigned int uint_param = 10000;
static unsigned long ulong_param = 1000000;
static char *charp_param = "This is a charp param";
static int int_array[5] = {0, 1, 2, 3, 4};
static unsigned int arr_num = 5;

/**
 * @page p2
 * @section p2s3 Step 3: Make the parameters available in the file system
 *
 * We have to make the parameters available in the Linux file system.
 * That can be done by using the macro `module_param`:
 *
 * @code
 * module_param(short_param, short, S_IRUGO | S_IWUSR);
 * module_param(int_param, int, S_IRUGO | S_IWUSR);
 * module_param(long_param, long, S_IRUGO | S_IWUSR);
 * module_param(ushort_param, ushort, S_IRUGO | S_IWUSR);
 * module_param(uint_param, uint, S_IRUGO | S_IWUSR);
 * module_param(ulong_param, ulong, S_IRUGO | S_IWUSR);
 * module_param(charp_param, charp, S_IRUGO | S_IWUSR);
 * module_param_array(int_array, int, &arr_num, S_IRUGO | S_IWUSR);
 * @endcode
 *
 * By using this macro the parameters will be exported to `sysfs`:
 * `/sys/module/mod_param/parameters/`
 *
 * The first argument is the name of the parameter, the second argument is the
 * type of the parameter, and the third argument represents the file permission.
 * If the file permission is set to zero, the parameter is not shown in `sysfs`.
 */

/**
 * @name module_param
 * @brief Makes the parameters available in the file system.
 * @details By using this macro the parameters will be exported to sysfs:
 *     /sys/module/<module_name>/parameters/
 * @param name Name of the parameter
 * @param type Parameter data type
 * @param perm Permissions in sysfs
 */
/**@{*/
module_param(short_param, short, S_IRUGO | S_IWUSR);
module_param(int_param, int, S_IRUGO | S_IWUSR);
module_param(long_param, long, S_IRUGO | S_IWUSR);
module_param(ushort_param, ushort, S_IRUGO | S_IWUSR);
module_param(uint_param, uint, S_IRUGO | S_IWUSR);
module_param(ulong_param, ulong, S_IRUGO | S_IWUSR);
module_param(charp_param, charp, S_IRUGO | S_IWUSR);
/**@}*/

/**
 * @name module_param_array
 * @brief Makes the parameters available in the file system.
 * @details By using this macro the parameters will be exported to sysfs:
 *     /sys/module/<module_name>/parameters/
 * @param name Name of the parameter
 * @param type Parameter data type
 * @param nump Pointer containing the numer of elements
 * @param perm Permissions in sysfs
 */
/**@{*/
module_param_array(int_array, int, &arr_num, S_IRUGO | S_IWUSR);
/**@}*/

/**
 * @page p2
 * @section p2s4 Step 4: Parameters description
 *
 * Optionally, you can write a description for the parameters.
 * Description will be shown in `modinfo`.
 *
 * @code
 * MODULE_PARM_DESC(short_param, "This is a short integer");
 * MODULE_PARM_DESC(int_param, "This is an integer");
 * MODULE_PARM_DESC(long_param, "This is a long integer");
 * MODULE_PARM_DESC(ushort_param, "This is an unsigned short integer");
 * MODULE_PARM_DESC(uint_param, "This is an unsigned integer");
 * MODULE_PARM_DESC(ulong_param, "This is an unsigned long integer");
 * MODULE_PARM_DESC(charp_param, "This is a char pointer");
 * MODULE_PARM_DESC(int_array, "This is an array of integers");
 * @endcode
 */
/**
 * @name MODULE_PARM_DESC
 * @brief Set the parameters description.
 * @details Optionally you can write a description for the parameters.
 * Description will be shown when executing `modinfo`.
 * @param name Name of the parameter
 * @param desc Brief description of the parameter
 */
/**@{*/
MODULE_PARM_DESC(short_param, "This is a short integer");
MODULE_PARM_DESC(int_param, "This is an integer");
MODULE_PARM_DESC(long_param, "This is a long integer");
MODULE_PARM_DESC(ushort_param, "This is an unsigned short integer");
MODULE_PARM_DESC(uint_param, "This is an unsigned integer");
MODULE_PARM_DESC(ulong_param, "This is an unsigned long integer");
MODULE_PARM_DESC(charp_param, "This is a char pointer");
MODULE_PARM_DESC(int_array, "This is an array of integers");
/**@}*/

/**
 * @page p2
 * @section p2s5 Step 5: Print the parameters value during initialization
 *
 * Print the values of all the module parameters inside the `mod_param_init`
 * function. Use the macro `pr_info` which behaves just like the
 * `printf` function:
 *
 * @code
 * pr_info("%s: Module Parameters:\n", __func__);
 * pr_info("short  = %d\n", short_param);
 * @endcode
 *
 * The parameters will be printed when the module is loaded with `insmod`.
 *
 */

/**
 * @brief Initialization function.
 * @details The module init function is called when insmod is executed.
 * This function initializes the device and registers it with the kernel.
 * @return Return `0` when init function is successful.
 */
static int __init mod_param_init(void)
{
	int i;

	/* Print module parameters */
	pr_info("%s: Module Parameters:\n", __func__);
	pr_info("short  = %d\n", short_param);
	pr_info("int    = %d\n", int_param);
	pr_info("long   = %ld\n", long_param);
	pr_info("ushort = %u\n", ushort_param);
	pr_info("uint   = %u\n", uint_param);
	pr_info("ulong  = %lu\n", ulong_param);
	pr_info("charp  = %s\n", charp_param);
	for (i = 0; i < arr_num; i++)
		pr_info("int_array[%d] = %d\n", i, int_array[i]);

	return 0;
}

/**
 * @page p2
 * @section p2s6 Step 6: Print the parameters value during cleanup
 *
 * Print the values of all the module parameters inside the `mod_param_exit`
 * function. Use the macro `pr_info` which behaves just like the
 * `printf` function:
 *
 * @code
 *	pr_info("%s: Module Parameters:\n", __func__);
 *	pr_info("short  = %d\n", short_param);
 * @endcode
 *
 */

/**
 * @brief Cleanup function.
 * @details The module exit function is called when `rmmod` is executed.
 * This function undoes whatever the initialization function did.
 */
static void __exit mod_param_exit(void)
{
	int i;

	/* Print module parameters */
	pr_info("%s: Module Parameters:\n", __func__);
	pr_info("short  = %d\n", short_param);
	pr_info("int    = %d\n", int_param);
	pr_info("long   = %ld\n", long_param);
	pr_info("ushort = %u\n", ushort_param);
	pr_info("uint   = %u\n", uint_param);
	pr_info("ulong  = %lu\n", ulong_param);
	pr_info("charp  = %s\n", charp_param);
	for (i = 0; i < arr_num; i++)
		pr_info("int_array[%d] = %d\n", i, int_array[i]);
}

/**
 * @page p2
 * @section p2s7 Step 7: Tell the kernel which are your initialization and
 * cleanup functions
 *
 * Remember, the `module_init` and `module_exit` macros tell the kernel which
 * functions will be used to initialize the module when it is loaded by
 * `insmod` and which function will clean and release the resources acquired
 * during initializtion when the module is removed by `rmmod`:
 *
 * @code
 * module_init(mod_param_init);
 * module_exit(mod_param_exit);
 * @endcode
 */
module_init(mod_param_init);
module_exit(mod_param_exit);

/**
 * @page p2
 * @section p2s8 Step 8: Module license
 *
 * Remember we have to tell the kernel which license is used by the module
 *
 * @code
 * MODULE_LICENSE("GPL");
 * @endcode
 */
MODULE_LICENSE("GPL");

/**
 * @page p2
 * @section p2s9 Step 9: Module information
 *
 * Optionally, you can give more information about the author of the module and
 * add a brief module description. This information is retrieved by `modinfo`.
 *
 * @code
 * MODULE_AUTHOR("Your Name Here <your.email@here.com>");
 * MODULE_DESCRIPTION("Shows the usage of module parameters");
 * @endcode
 */
MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
MODULE_AUTHOR("Adrian Ortega Garcia <adrianog.sw@gmail.com>");
MODULE_DESCRIPTION("Shows the usage of module parameters");

/**
 * @page p2
 * @section p2s10 Step 10: How to pass arguments to this module
 *
 * To insert the module without modifying parameters, use on the shell:
 *
 * @code
 * $> insmod mod_param.ko
 * @endcode
 *
 * To pass command line arguments to this module:
 *
 * @code
 * $> insmod mod_param.ko int_param=15 charp_param="hello" int_array=1,2,3
 * @endcode
 *
 * Note: To insert a module, you have to be super user.
 */

/**
 * @page p2
 * @section p2s11 Summary
 *
 * After following all the steps described, your source code should look similar
 * to this:
 *
 * @include mod_param.c
 */
