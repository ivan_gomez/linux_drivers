/**
 * @file char_drv.c
 * @brief Character driver
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 * @author Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *}
 */

/**
 * @page p3 Lab 03: Character driver
 *
 * In this lab you will learn how to write a character driver in linux. This
 * is one of the most basic driver types one can write.
 *
 * For this specific lab, our char driver will use a buffer in kernel space: An
 * application will be able to write data to this buffer by using the `write`
 * system call, and whenever the application asks to read, by usin the `read`
 * system call, it will read whatever data has been writen to the buffer. If no
 * data is available, nothing will be read.
 */

/**
 * @page p3
 * @section p3s1 Step 1: Add the necessary header files
 *
 * Like in previous labs, we will need to include a set of header files to
 * implement our char driver.
 *
 * @code
 * #include <linux/module.h>
 * #include <linux/fs.h>
 * #include <linux/cdev.h>
 * #include <linux/device.h>
 * #include <linux/uaccess.h>
 * @endcode
 *
 * The header file `linux/module.h` handles dynamic loading of modules into the
 * kernel. It defines the functions `module_init` and `module_exit`, and the
 * macros `MODULE_AUTHOR`, `MODULE_LICENSE`, etc. The header file `linux/fs.h`
 * contains definitions of important data structures, such as `file` and
 * `file_operations`. The header file `linux/cdev.h` defines the structure
 * `cdev` and the functions `cdev_init` and `cdev_add`. The header file
 * `linux/device.h` is needed to create the device and class with the functions
 * `device_create` and `class_create`. The header file `linux/uaccess.h` is used
 * to access user-space with `copy_from_user` and `copy_to_user`, among other
 * functions.
 */
#include <linux/module.h>	/* Dynamic loading of modules into the kernel */
#include <linux/fs.h>		/* file defs of important data structures */
#include <linux/cdev.h>		/* char devices structure and aux functions */
#include <linux/device.h>	/* generic, centralized driver model */
#include <linux/uaccess.h>	/* for accessing user-space */

/**
 * @page p3
 * @section p3s2 Step 2: Declare all the needed global variables and macros
 *
 * Define a macro which we will use for our buffer size inside the kernel:
 *
 * @code
 * #define KBUFF_MAX_SIZE	1024
 * @endcode
 *
 * Define a macro which will be substituted by a string containing the device
 * name:
 *
 * @code
 * #define DEVICE_NAME	"mydev"
 * @endcode
 *
 * Declare an integer to hold the value of the major number:
 *
 * @code
 * static unsigned major;
 * @endcode
 *
 * Define a static global variable of the type `struct cdev` which will be used
 * to register the char device with the kernel:
 *
 * @code
 * static struct cdev mydev;
 * @endcode
 *
 * Now we need a pointer to `struct class`, which is used to create the device
 * class shown in `/sys/class/`. Device class is needed to create the device
 * node under `/dev/`.
 *
 * @code
 * static struct class *mydev_class;
 * @endcode
 *
 * Then declare a pointer to `struct device`, which will be used to create the
 * device node under /dev/.
 *
 * @code
 * static struct device *mydev_device;
 * @endcode
 *
 * Define the kernel buffer as an array of chars of `KBUFF_MAX_SIZE` elements:
 *
 * @code
 * static char kbuffer[KBUFF_MAX_SIZE];
 * @endcode
 *
 * Declare a variable to know how many bytes have been written in the kernel
 * buffer. Every time there is a `write` request, we will increment this
 * variable, and when there is a `read` request, we will decrement it.
 *
 * @code
 * static unsigned long kbuffer_size;
 * @endcode
 *
 * Do not confuse `kbuffer_size` with `KBUFF_MAX_SIZE`. `KBUFF_MAX_SIZE`
 * represents the maximum size of the `kbuffer` array, while `kbuffer_size`
 * represents how many bytes are written to this array.
 */
#define KBUFF_MAX_SIZE	1024	/* size of the kernel side buffer */
#define DEVICE_NAME	"mydev"

static unsigned major;			/* major number */
static struct cdev mydev;		/* to register with kernel */
static struct class *mydev_class;	/* to register with sysfs */
static struct device *mydev_device;	/* to register with sysfs */

static char kbuffer[KBUFF_MAX_SIZE];	/* kernel side buffer */
static unsigned long kbuffer_size;	/* kernel buffer length */

/**
 * @page p3
 * @section p3s3 Step 3: Implement the read function
 *
 * Write a function which has the following prototype:
 *
 * @code
 * static ssize_t mydev_read(struct file *filp, char __user *buffer, size_t nbuf, loff_t *offset);
 * @endcode
 *
 * The parameter `filp` is a pointer to the `file` structure, which contains
 * file information such as the `file_operations`, `f_mode` (e.g. `O_RDWR`),
 * `f_flags` (e.g. `O_NONBLOCK`), etc. The parameter `buffer` is a pointer to
 * the buffer in the user space, which is different from the `kbuffer` used by
 * this driver. The parameter `nbuf` specifies how many bytes of data are
 * requested to read. The parameter `offset` specifies the position in the
 * current file descriptor.
 *
 * Inside this function you will have to follow this algorithm:
 *
 * @par Steps\n
 * @parblock
 * 1 - Check if there is data available to read by using the variables `offset`
 * and `kbuffer_size`. If the `offset` is greater than `kbuffer_size` just exit
 * the function and return 0.
 *
 * 2 - Check for the maximum size of the kernel buffer. If the user wants to
 * read more data than available, adjust the value of the variable `nbuf`.
 *
 * 3 - Copy the data from the driver buffer `kbuffer` to the user space `buffer`
 * by using the function:
 *
 * @code
 * unsigned long copy_to_user(void __user *to, const void *from, unsigned long n);
 * @endcode
 *
 * 4 - Check the value returned by this function. If a non-zero value is
 * returned then return the value -EFAULT.
 *
 * 5 - If the function returned a 0 value, update the value of `offset` with the
 * variable `nbuf`. Make sure the function returns the amount of bytes written
 * into the user space buffer using the `nbuf` variable.
 * @parblockend
 */
static ssize_t mydev_read(struct file *filp, char __user *buffer, size_t nbuf,
								loff_t *offset)
{
	int ret = 0;
	pr_info("[%d] %s\n", __LINE__, __func__);
	pr_info("[%d] nbuf = %d, off = %d", __LINE__, (int)nbuf, (int)*offset);
	/* If offset is greater than kbuffer size, there is nothing to copy */
	if (*offset >= kbuffer_size) {
		pr_info("Nothing to read: Offset = %d, kbuffer_size = %ld",
						(int)*offset, kbuffer_size);
		goto out;
	}

	/* Check for maximum size of kernel buffer */
	if ((nbuf + *offset) > kbuffer_size)
		nbuf = kbuffer_size - *offset;

	/* fill the buffer, return the buffer size */
	pr_info("[%d] Copying Buffer from kernel to user space...\n", __LINE__);
	ret = copy_to_user(buffer, &kbuffer[*offset], nbuf);
	if (ret) {
		pr_info("copy_to_user failed: 0x%x", ret);
		ret = -EFAULT;
		goto out;
	}

	*offset += nbuf;
	ret = nbuf;

out:
	pr_info("[%d] nbuf = %d, offset = %d",
					__LINE__, (int)nbuf, (int)*offset);
	return ret;
}

/**
 * @page p3
 * @section p3s4 Step 4: Implement the write function
 *
 * Write a function which has the following prototype:
 *
 * @code
 * static ssize_t mydev_write(struct file *filp, const char __user *buffer, size_t nbuf, loff_t *offset);
 * @endcode
 *
 * The parameter `filp` is a pointer to the `file` structure, which contains
 * file information such as the `file_operations`, `f_mode` (e.g. `O_RDWR`),
 * `f_flags` (e.g. `O_NONBLOCK`), etc. The parameter `buffer` is a pointer to
 * the buffer in the user space, which is different from the `kbuffer` used by
 * this driver. The parameter `nbuf` specifies how many bytes of data are
 * requested to write. The parameter `offset` specifies the position in the
 * current file descriptor.
 *
 * Inside this function you will have to follow this algorithm:
 *
 * @par Steps\n
 * @parblock
 * 1 - Check if there is space available to write by using the variable `offset`
 * and the macro `KBUFF_MAX_SIZE`. If the `offset` is greater or equal to
 * `KBUFF_MAX_SIZE` print a message stating there is no space to write and exit
 * the function returning -ENOMEM or a negative value.
 *
 * 2 - Check for the available space in the kernel buffer. If the user wants to
 * write more data than the available space, adjust the value of the variable
 * `nbuf`.
 *
 * 3 - Copy the data to the driver buffer `kbuffer` from the user space `buffer`
 * by using the function:
 *
 * @code
 * unsigned long copy_from_user(void *to, const void __user *from, unsigned long n);
 * @endcode
 *
 * 4 - Check the value returned by this function. If a non-zero value is
 * returned then return the value -EFAULT.
 *
 * 5 - If the function returned a 0 value, update the value of `offset` with the
 * variable `nbuf`. Also update the value of the variable `kbuffer_size` with
 * the new `offset`. Make sure the function returns the amount of bytes written
 * into the kernel-space buffer by using the `nbuf` variable.
 * @parblockend
 */
static ssize_t mydev_write(struct file *filp, const char __user *buffer,
						size_t nbuf, loff_t *offset)
{
	int ret = 0;

	pr_info("[%d] %s\n", __LINE__, __func__);
	pr_info("[%d] nbuf = %d, offset = %d",
					__LINE__, (int)nbuf, (int)*offset);

	if (*offset >= KBUFF_MAX_SIZE) {
		pr_info("No space to write: Offset = %d, kbuffer_size = %ld",
						(int)*offset, kbuffer_size);
		ret = -ENOMEM;
		goto out;
	}

	/* Check for maximum size of kernel buffer */
	if ((nbuf + *offset) > KBUFF_MAX_SIZE)
		nbuf = KBUFF_MAX_SIZE - *offset;

	/* fill the buffer, return the buffer size */
	pr_info("[%d] Copying Buffer from user to kernel space...\n", __LINE__);
	ret = copy_from_user(&kbuffer[*offset], buffer, nbuf);
	if (ret) {
		pr_info("copy_from_user failed: 0x%x", ret);
		ret = -EFAULT;
		goto out;
	}

	*offset += nbuf;
	kbuffer_size = *offset;
	ret = nbuf;

out:
	pr_info("[%d] nbuf = %d, offset = %d",
					__LINE__, (int)nbuf, (int)*offset);
	return ret;
}

/**
 * @page p3
 * @section p3s5 Step 5: Implement the open function
 *
 * Write a function with the following prototype:
 *
 * @code
 * static int mydev_open(struct inode *ip, struct file *filp);
 * @endcode
 *
 * Don't worry about the parameters, in this function just print a message
 * saying your function was called and return 0;
 */
static int mydev_open(struct inode *ip, struct file *filp)
{

	pr_info("Driver's open function was called\n");

	return 0;
}

/**
 * @page p3
 * @section p3s6 Step 6: Implement the close function
 *
 * Write a function with the following prototype:
 *
 * @code
 * static int mydev_close(struct inode *ip, struct file *filp);
 * @endcode
 *
 * Don't worry about the parameters, in this function just print a message
 * saying your function was called and return 0;
 */
static int mydev_close(struct inode *ip, struct file *filp)
{

	pr_info("Driver's close function was called\n");

	return 0;
}


/**
 * @page p3
 * @section p3s7 Step 7: Define the operation your device supports
 *
 * Declare a static global variable of type `struct file_operations`
 * and initialize it with the functions we have just defined, i.e. the read,
 * write, open, and close functions.
 *
 * @code
 * static const struct file_operations mydev_fops = {
 *	.owner = THIS_MODULE,
 *	.read = mydev_read,
 *	.write = mydev_write,
 *	.open = mydev_open,
 *	.release = mydev_close
 *};
 * @endcode
 *
 */
static const struct file_operations mydev_fops = {
	.owner = THIS_MODULE,
	.read = mydev_read,
	.write = mydev_write,
	.open = mydev_open,
	.release = mydev_close
};

/**
 * @page p3
 * @section p3s8 Step 8: Define the initialization function
 *
 * Write a function with the following prototype:
 *
 * @code
 * static int __init mymodule_init(void);
 * @endcode
 *
 * Inside this function you will have to do the following:
 *
 * @par Steps\n
 * @parblock
 * 1 - Allocate the MAJOR and MINOR numbers with the function:
 * @code
 * int alloc_chrdev_region(dev_t *dev, unsigned baseminor, unsigned count, const char *name);
 * @endcode
 * If a non-zero value is returned it means failure and you have to return a
 * negative number.
 *
 * 2 - Save the major number by using the macro:
 * @code
 * MAJOR(devid);
 * @endcode
 *
 * 3 - Initialize the `cdev` structure by using the function:
 * @code
 * void cdev_init(struct cdev *cdev, struct file_operations *fops);
 * @endcode
 *
 * 4 - Register the char device with the kernel by using the function:
 * @code
 * int cdev_add(struct cdev *dev, dev_t num, unsigned int count);
 * @endcode
 * If it fails (a non-zero value is returned), release the major and minor
 * numbers with:
 * @code
 * void unregister_chrdev_region(dev_t from, unsigned count);
 * @endcode
 * and return a negative value.
 *
 * 5 - Create the device class (in `/sys/class/`) by using:
 * @code
 * struct class *class_create(owner, name);
 * @endcode
 * Set `owner` to `THIS_MODULE`, and `name` is the one you want to be shown in
 * `/sys/class/`. If `NULL` is returned, skip the next step since it needs this
 * pointer. Failure here is not fatal.
 *
 * 6 - Create the device node (in `/dev/`) with the function:
 * @code
 * struct device *device_create(struct class *cls,
 *				struct device *parent,
 *				dev_t devt,
 *				void *drvdata,
 *				const char *fmt, ...);
 * @endcode
 * you can set `parent` and `drvdata` to `NULL`. `fmt` is the name you want to
 * show in `/dev/`. You can use `DEVICE_NAME` here to create `/dev/mydev0`.
 *
 * 7 - Print a message saying that the driver was loaded.
 * @parblockend
 */
static int __init mymodule_init(void)
{
	int ret;
	dev_t devid;

	/* Dynamic allocation of MAJOR and MINOR numbers */
	ret = alloc_chrdev_region(&devid, 0, 1, DEVICE_NAME);

	if (ret) {
		pr_err("Error: Failed registering major number\n");
		return ret;
	}

	/* save MAJOR number */
	major = MAJOR(devid);

	/* Initalize the cdev structure */
	cdev_init(&mydev, &mydev_fops);

	/* Register the char device with the kernel */
	ret = cdev_add(&mydev, devid, 1);
	if (ret) {
		pr_err("Error: Failed registering with the kernel\n");
		unregister_chrdev_region(devid, 1);
		return ret;
	}

	/* Create a class and register with the sysfs. (Failure is not fatal) */
	mydev_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(mydev_class)) {
		pr_info("class_create() failed: %ld\n", PTR_ERR(mydev_class));
		mydev_class = NULL;
	} else {
		/* Register device with sysfs (creates device node in /dev) */
		mydev_device = device_create(mydev_class, NULL, devid, NULL,
								DEVICE_NAME"0");
		if (IS_ERR(mydev_device)) {
			pr_info("device_create() failed: %ld\n",
							PTR_ERR(mydev_device));
			mydev_device = NULL;
		}
	}

	pr_info("Driver \"%s\" loaded...\n", DEVICE_NAME);
	return 0;
}

/**
 * @page p3
 * @section p3s9 Step 9: Define the exit function
 *
 * In this function we will do the opposite operations done in the
 * initialization function. Write a function with the following prototype:
 *
 * @code
 * static void __exit mymodule_exit(void);
 * @endcode
 *
 * The exit function has to undo whatever the init function did:
 * @par Steps\n
 * @parblock
 * 1 - Deregister char device from kernel by using the function:
 * @code
 * void cdev_del(struct cdev *dev);
 * @endcode
 *
 * 2 - Release MAJOR and MINOR numbers with the function:
 * @code
 * void unregister_chrdev_region(dev_t from, unsigned count);
 * @endcode
 *
 * 3 - Deregister device from `devfs` with the function:
 * @code
 * void device_destroy(struct class *cls, dev_t devt);
 * @endcode
 *
 * 4 - Deregister class from `sysfs` with the function:
 * @code
 * void class_destroy(struct class *cls);
 * @endcode
 *
 * 5 - Print a message saying that the module was removed.
 * @parblockend
 */
static void __exit mymodule_exit(void)
{
	/* Deregister char device from kernel */
	cdev_del(&mydev);

	/* Release MAJOR and MINOR numbers */
	unregister_chrdev_region(MKDEV(major, 0), 1);

	/* Deregister device from sysfs */
	if (mydev_class != NULL) {
		device_destroy(mydev_class, MKDEV(major, 0));
		class_destroy(mydev_class);
	}

	pr_info("Driver \"%s\" unloaded...\n", DEVICE_NAME);
}

/**
 * @page p3
 * @section p3s10 Step 10: Boilerplate code
 *
 * From previous labs you should remember what the following macros do.
 *
 * @code
 * module_init(mymodule_init);
 * module_exit(mymodule_exit);
 *
 * MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
 * MODULE_AUTHOR("Adrian Ortega Garcia <adrianog.sw@gmail.com>");
 * MODULE_LICENSE("GPL");
 * MODULE_DESCRIPTION("Character Driver.");
 * @endcode
 */
module_init(mymodule_init);
module_exit(mymodule_exit);

MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
MODULE_AUTHOR("Adrian Ortega Garcia <adrianog.sw@gmail.com>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Character Driver.");

/**
 * @page p3
 * @section p3s11 Summary
 *
 * After following all the steps described, your source code should look similar
 * to this:
 *
 * @include char_drv.c
 */
