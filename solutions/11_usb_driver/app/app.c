#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "missile_ioctl.h"

/* This app opens the /dev/skel0 device */
#define BUFF_SIZE	10

int send_cmd_to_device(char *dev_name, int cmd, void *arg)
{
	int fd;
	int rc;
	int duration;

	if (cmd == MISSILE_IOC_SHOOT)
		duration = 3000000;
	else if ((cmd == MISSILE_IOC_DOWN) || (cmd == MISSILE_IOC_UP))
		duration = 400000;
	else
		duration = 800000;

	fd = open(dev_name, O_RDWR);
	if (fd < 0) {
		printf("Error while opening %s\n", dev_name);
		return fd;
	}
	printf("Opened %s\n", dev_name);

	rc = ioctl(fd, cmd, arg);
	if (rc < 0) {
		printf("Error while sending IOCTL command:%d\n", rc);
		return rc;
	}
	printf("IOCTL: %d\n", cmd);

	printf("duration = %d\n", duration);
	usleep(duration);
	rc = ioctl(fd, MISSILE_IOC_STOP, arg);
	if (rc < 0) {
		printf("Error while sending IOCTL command:%d\n", rc);
		return rc;
	}
	printf("IOCTL: %d\n", MISSILE_IOC_STOP);

	rc = close(fd);
	if (rc < 0) {
		printf("Error while closing the device\n");
		return rc;
	}
	printf("Closed %s\n", dev_name);

	return 0;
}

void pr_help(char *prog_name)
{
	printf("Usage:\n");
	printf("%s <device_name>\n", prog_name);
}

int main(int argc, char *argv[])
{
	int rc;
	char *device_name;

	if (argc == 2) {
		device_name = argv[1];
	} else {
		pr_help(argv[0]);
		return -1;
	}

	rc = send_cmd_to_device(device_name, MISSILE_IOC_DOWN, NULL);
	if (rc) {
		printf("Failed to send MISSILE_IOC_DOWN command\n");
		return rc;
	}

	rc = send_cmd_to_device(device_name, MISSILE_IOC_UP, NULL);
	if (rc) {
		printf("Failed to send MISSILE_IOC_UP command\n");
		return rc;
	}

	rc = send_cmd_to_device(device_name, MISSILE_IOC_LEFT, NULL);
	if (rc) {
		printf("Failed to send MISSILE_IOC_LEFT command\n");
		return rc;
	}

	rc = send_cmd_to_device(device_name, MISSILE_IOC_RIGHT, NULL);
	if (rc) {
		printf("Failed to send MISSILE_IOC_RIGHT command\n");
		return rc;
	}

	rc = send_cmd_to_device(device_name, MISSILE_IOC_SHOOT, NULL);
	if (rc) {
		printf("Failed to send MISSILE_IOC_SHOOT command\n");
		return rc;
	}

	return 0;
}
