/**
 * @mainpage Linux Device Drivers course
 *
 * In this course you will learn how to write your own Linux device drivers.
 *
 * @tableofcontents
 *
 */

/**
 * @file hello.c
 * @brief Hello World Module.
 * @details This is a simple Linux kernel module which prints a message in
 * the Linux kernel logging system.
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 * @author Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *}
 */

/**
 * @page p1 Lab 01: Hello World module
 *
 * @tableofcontents
 *
 * This lab contains the all the necessary steps to write a simple Linux kernel
 * module, which prints a message in the Linux kernel logging system.
 *
 */

/**
 * @page p1
 * @section s01 Step 1: Add support to load and unload kernel modules
 *
 * Include the `linux/module.h` header file to add support for loading and
 * unloading kernel modules.
 *
 * @code
 *	#include <linux/module.h>
 * @endcode
 */
#include <linux/module.h>

/**
 * @page p1
 * @section s02 Step 2: Initialization function
 *
 * The module init function is called when `insmod` is executed.
 *
 * Write a function with the following prototype:
 *
 * @code
 *  static int __init hello_init(void)
 * @endcode
 *
 * Inside this function print a friendly message using the macro:
 *
 * @code
 *  pr_info
 * @endcode
 *
 * The `pr_info` macro behaves like the `printf` function. i.e.
 *
 * @code
 *  pr_info("Hello world");
 * @endcode
 */

/**
 * @brief Initialization function.
 * @details The module init function is called when `insmod` is executed.
 * This function initializes the device and registers it with the kernel.
 * @return Return `0` when init function is successful.
 */
static int __init hello_init(void)
{
	pr_info("%s(%d): Hello World!!!\n", __func__, __LINE__);

	return 0;
}

/**
 * @page p1
 * @section s03 Step 3: The cleanup function
 *
 * The module exit function is called when `rmmod` is executed.
 *
 * Write a function with the following prototype:
 *
 * @code
 *  static void __exit hello_exit(void)
 * @endcode
 *
 * Inside this function print a farewell message using the macro:
 *
 * @code
 *  pr_info
 * @endcode
 *
 * The `pr_info` macro behaves like the `printf` function.
 *
 * @code
 *  pr_info("Goodbye cruel world!");
 * @endcode
 */

/**
 * @brief Cleanup function.
 * @details The module exit function is called when `rmmod` is executed.
 * This function undoes whatever the initialization function did.
 */
static void __exit hello_exit(void)
{
	pr_info("%s(%d): Goodbye!!!\n", __func__, __LINE__);
}

/**
 * @page p1
 * @section s04 Step 4: Connecting the initialization function with the kernel
 *
 * We have to tell the Linux kernel which function to call when `insmod`
 * is executed. This is done by using the macro `module_init` and passing the
 * function address we want to be executed.
 *
 * Write the macro `module_init` and pass the function address to it. i.e.
 *
 * @code
 *  module_init(hello_init);
 * @endcode
 */

/**
 * @brief Driver initialization entry point.
 * @details We have to tell the Linux kernel which function to call when
 * `insmod` is executed. This is done by using the macro `module_init` and
 * passing the function address we want to be executed.
 * @param Initialization_Function
 */
module_init(hello_init);

/**
 * @page p1
 * @section s05 Step 5: Connecting the cleanup function with the kernel
 *
 * We have to tell the Linux kernel which function to call when `rmmod`
 * is executed. This is done by the macro `module_exit` and passing the
 * function address we want to be executed.
 *
 * Write the macro `module_exit` and pass the function address to it. i.e.
 *
 * @code
 *  module_exit(hello_exit);
 * @endcode
 */

/**
 * @brief Driver exit entry point.
 * @details We have to tell the Linux kernel which function to call when `rmmod`
 * is executed. This is done by the macro `module_exit` and passing the
 * function address we want to be executed.
 * @param Cleanup_Function
 */
module_exit(hello_exit);

/**
 * @page p1
 * @section s06 Step 6: Module license
 *
 * Let know the kernel which license you are using for your module by
 * writing the following macro:
 *
 * @code
 *  MODULE_LICENSE("GPL");
 * @endcode
 */

/**
 * @brief Module license.
 * @details Let know the kernel which license you are using for your module by
 * writing the following macro.
 */
MODULE_LICENSE("GPL");

/**
 * @page p1
 * @section s07 Step 7: Module information
 *
 * Optionally, you can add a module author and a brief description with the
 * following macros:
 *
 * @code
 *  MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
 *  MODULE_DESCRIPTION("Hello World Module");
 * @endcode
 */

MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
MODULE_AUTHOR("Adrian Ortega Garcia <adrianog.sw@gmail.com>");
MODULE_DESCRIPTION("Hello World Module");

/**
 * @page p1
 * @section s08 Summary
 *
 * After following all the steps described, your source code should look similar
 * to this:
 *
 * @include hello.c
 */

/**
 * @page p1
 * @section s09 Additional content: Considerations about __init.
 *
 * Let's see what are the implications of the `__init` keyword.
 *
 * @parblock
 * 1 - In the `hello_init` function of hello.c remove the `__init` keyword and
 * print the address of the function `hello_init`.
 *
 * @code
 *  pr_info("Address of hello_init = 0x%p\n", &hello_init);
 * @endcode
 *
 * 2 - Create another module named func_param.c with the same content of
 * hello.c. Modify its init function to be able to call the `hello_init`
 * function of the hello.c module: Declare a function pointer and set it to
 * the address printed by the `hello` module (you can use a module parameter
 * to pass the address of the `hello_init` function to the `func_param` module
 * at insertion time).
 *
 * @code
 * int(* myfunc) (void);
 * if (func_addr) {
 *	pr_info("%s(%d): Calling function at func_addr = 0x%lX\n",
 *					__func__, __LINE__, func_addr);
 *	myfunc = (void *)func_addr;
 *	myfunc();
 * }
 * @endcode
 *
 * 3 - Modify the Makefile to compile both modules (hello.c and func_param.c)
 * and build them.
 *
 * 4 - Insert the hello.ko module, read the address of the `hello_init` function
 * printed in `dmesg`.
 *
 * 5 - If you didn't use a module parameter in func_param.c then set the
 * `myfunc` pointer to the address printed in step 4, build the module, and
 * instert it. If you used a module parameter then just insert the
 * `func_param.ko` module in the following way:
 *
 * @code
 *  sudo insmod func_param.ko func_addr=<function_address>
 * @endcode
 * @parblockend
 *
 * Check the `dmesg` logs, you will see the messages from `func_param` and then
 * the messages from `hello` module. That means that you were able to call a
 * function of the `hello` module from the `func_param` module.
 *
 * When you use the `__init` keyword, it indicates that this function can be
 * called only when the module inserted. Let's see what happens when you try
 * to call the `hello_init` function when `__init` is used:
 *
 * Modify the hello.c file again, and add the `__init` back to the `hello_init`
 * function. Build it and insert it, and then insert the `func_param` module
 * (make sure that you use the new function address when inserting this module).
 * See what happens, check the dmesg to see the kernel logs. (WARNING: after
 * doing this, you won't be able to remove the module).
 */
