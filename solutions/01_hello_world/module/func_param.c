/**
 * @file func_param.c
 * @brief This module calls a function passed as a module parameter.
 * @details This module receives a function address as a module parameter,
 * then it tries to call it.
 *
 * Usage: sudo insmod func_param.ko func_addr=<func_addr>
 *
 * WARNING: Only pass a valid function address.
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *}
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>

static unsigned long func_addr = 0;
module_param(func_addr, ulong, S_IRUGO);
MODULE_PARM_DESC(func_addr, "Address of the function to be called");

static int __init func_param_init(void)
{
	int(* myfunc) (void);

	pr_info("%s(%d): Hello world!!!\n", __func__, __LINE__);

	if (func_addr) {
		pr_info("%s(%d): Calling function at func_addr = 0x%lX\n",
						__func__, __LINE__, func_addr);
		myfunc = (void *)func_addr;
		myfunc();
	}

	return 0;
}

static void __exit func_param_exit(void)
{
	pr_info("%s(%d): Goodbye!!!\n", __func__, __LINE__);
}

module_init(func_param_init);
module_exit(func_param_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
MODULE_DESCRIPTION("This module calls a function located at func_addr");
