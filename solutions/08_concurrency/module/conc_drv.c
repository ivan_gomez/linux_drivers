/**
 * @file conc_drv.c
 * @brief Synchronization Methods for Concurrency management
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * }
 */

/**
 * @page p8 Lab 08: Concurrency
 *
 * In this lab you will learn how to deal with concurrency issues by using
 * synchronization mechanisms such as mutex and semaphores.
 *
 * Until this lab, we haven't paid much attention to concurrency issues in our
 * driver. We always use ONLY one application to write data to the device (i.e.
 * `echo`) and ONLY one application to read data from the device (i.e. `cat`).
 * But what happens if you use more than one application to try to write data
 * to the device simultaneously? (i.e. open multiple instances of `echo`, or
 * use a multithreaded app where every thread tries to wite data to the
 * device). What would happen if there are multiple applications trying to read
 * simultaneously from the device?
 *
 * This lab will help you to fix those issues.
 */

/**
 * @page p8
 * @section p8s1 Step 1: The module header files
 *
 * Edit the header file `conc_drv.h`, and add a macro to define the maximun
 * number of opened files. That is, if there are multiple applications trying
 * to open the device, only the first `MAX_OPEN_DEVS` will be able to open the
 * device, the rest will get an error when trying to open the device. By
 * default lets allow a maximun number of 4 applications to open the device
 * simultaneously.
 *
 * @code
 * #define MAX_OPEN_DEVS	4
 * @endcode
 *
 * Now, add the mutexes and semaphore for each device in the structure `my_dev`
 *
 * @code
 * 	struct mutex read_mtx;
 * 	struct mutex write_mtx;
 * 	struct semaphore sem;
 * @endcode
 *
 * We will have two mutexes, one for the reader applications, and another for
 * the writer applications. That is, only one reader can be reading from the
 * ring buffer at a time and only one writer can be writing data to the ring
 * buffer at a time. But there can be one reader and one writer accessing the
 * ring buffer simultaneously.
 */

/**
 * @page p8
 * @section p8s2 Step 2: Add the necessary header files
 *
 * Now edit the `conc_drv.c` file. You will have to add the following header
 * files:
 *
 * @code
 * #include <linux/mutex.h>
 * #include <linux/semaphore.h>
 * @endcode
 *
 * The header file `linux/mutex.h` contains the mutex function definitions.
 * The header file `linux/semaphore.h` contains the definitions of the
 * semaphore functions.
 */


#include <linux/module.h>	/* Dynamic loading of modules into the kernel */
#include <linux/fs.h>		/* file defs of important data structures */
#include <linux/cdev.h>		/* char devices structure and aux functions */
#include <linux/device.h>	/* generic, centralized driver model */
#include <linux/uaccess.h>	/* for accessing user-space */
#include <linux/moduleparam.h>	/* Module parameters */
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/errno.h>	/* Error codes */
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/wait.h>		/* for wait_event and wake_up */
#include <linux/sched.h>	/* for TASK_INTERRUPTIBLE */
#include <linux/mm.h>		/* for memory mapping (mmap) */
#include <linux/delay.h>	/* for msleep */
#include <linux/mutex.h>	/* mutex */
#include <linux/semaphore.h>	/* semaphore */
#include "chardrv_ioctl.h"	/* IOCTL commands */
#include "conc_drv.h"

static unsigned major;			/* major number */
static unsigned first_minor;		/* first minor number */
static unsigned num_devices = 10;	/* number of devices */
static unsigned rb_size = KBUFF_SIZE;	/* ring buffer size */
static struct class *mydev_class;	/* to register with sysfs */
static struct device *mydev_device;	/* to register with sysfs */

struct my_dev *my_devices;	/* array of structs of num_devices elements */

/* Module parameters */
module_param(first_minor, uint, S_IRUGO);
module_param(num_devices, uint, S_IRUGO);
module_param(rb_size, uint, S_IRUGO);

MODULE_PARM_DESC(first_minor, "First minor number");
MODULE_PARM_DESC(num_devices, "Number of devices");
MODULE_PARM_DESC(rb_size, "Ring Buffer size");

/**
 * @page p8
 * @section p8s3 Step 3: Modify the read function
 *
 * Inside the read function and before atempting to access the shared resource
 * (the ring buffer) you have to make sure that you are the only one who have
 * access to it. To do it, we will use a mutex. If the mutex lock is acquired
 * by another thread then it will sleep until the lock becomes available.
 *
 * There is one consideration that we have to take into account: What happen if
 * the lock is already acquired and the application requested a NONBLOCKING
 * operation? In this case you cannot go to sleep! So instead of calling
 * `mutex_lock_interruptible` we have to try to acquire the lock by using the
 * function `mutex_trylock`. If this function returns one, it means that the
 * lock was acquired successfully, if it returns zero it means that the lock
 * was already acquired by another thread and in this case you have to return
 * an error immediately. The code will look like this: 
 *
 * @code
 * if (filp->f_flags & O_NONBLOCK) {
 *	if (!mutex_trylock(&dev->read_mtx))
 *		return -EAGAIN;
 * } else {
 *	if (mutex_lock_interruptible(&dev->read_mtx))
 *		return -EINTR;
 * }
 * @endcode
 *
 * Aditionally you have to replace all the `return` points by a `goto error`
 * statement. Make sure that the `ret` value contains a non zero value before
 * jumping to the `error` label. At the end of the read function, place the
 * `error` label, release the mutex lock, and return the `ret` value:
 * @code
 * error:
 *	mutex_unlock(&dev->read_mtx);
 *
 *	return ret;
 * @endcode
 */

/*
 * mydev_read()
 * This function implements the read function in the file operation structure.
 */
static ssize_t mydev_read(struct file *filp, char __user *buffer, size_t nbuf,
								loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (filp->f_flags & O_NONBLOCK) {
		/* NONBLOCK: return immediately if not able to get the lock */
		if (!mutex_trylock(&dev->read_mtx))
			return -EAGAIN;
	} else {
		if (mutex_lock_interruptible(&dev->read_mtx))
			return -EINTR;
	}

	if (ringbuffer_available_data(dev->rb) == 0) {
		/* If there is nothing to read and O_NONBLOCK is set, return */
		if (filp->f_flags & O_NONBLOCK) {
			pr_info("O_NONBLOCK flag set\n");
			ret = -EAGAIN;
			goto error;
		}

		/* Go to sleep until data is available */
		/* If interrupted by a signal, return -ERESTARTSYS */
		pr_info("%s: Going to sleep...\n", __func__);
		ret = wait_event_interruptible(dev->read_wq,
					ringbuffer_available_data(dev->rb) > 0);
		if (ret) {
			pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
			goto error;
		}
	}

	ret = access_ok(VERIFY_WRITE, buffer, nbuf);
	if (!ret) {
		pr_err("Invalid user address: %p\n", (void *)buffer);
		goto error;
	} else {
		ret = ringbuffer_read(dev->rb, buffer, nbuf);
	}

	/* Awake any writers */
	wake_up_interruptible(&dev->write_wq);

error:
	mutex_unlock(&dev->read_mtx);

	return ret;
}

/**
 * @page p8
 * @section p8s4 Step 4: Modify the write function
 *
 * Inside the write function and before atempting to access the shared resource
 * (the ring buffer) you have to make sure that you are the only one who have
 * access to it. To do it, we will use a mutex. If the mutex lock is acquired
 * by another thread then it will sleep until the lock becomes available.
 *
 * You have to do the same steps you did in the `read` function, except that
 * you have to use the write mutex `dev->write_mtx` instead of the read mutex
 * `dev->read_mtx`.
 */


/*
 * mydev_write()
 * This function implements the write function in the file operation structure.
 */
static ssize_t mydev_write(struct file *filp, const char __user *buffer,
						size_t nbuf, loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (filp->f_flags & O_NONBLOCK) {
		/* NONBLOCK: return immediately if not able to get the lock */
		if (!mutex_trylock(&dev->write_mtx))
			return -EAGAIN;
	} else {
		if (mutex_lock_interruptible(&dev->write_mtx))
			return -EINTR;
	}

	if (ringbuffer_available_space(dev->rb) == 0) {
		/* If the ring buffer is full and O_NONBLOCK is set, return */
		if (filp->f_flags & O_NONBLOCK) {
			pr_info("O_NONBLOCK flag set\n");
			ret = -EAGAIN;
			goto error;
		}

		/* Go to sleep until there is space in the ring buffer */
		/* If interrupted by a signal, return -ERESTARTSYS */
		pr_info("%s: Going to sleep...\n", __func__);
		ret = wait_event_interruptible(dev->write_wq,
				ringbuffer_available_space(dev->rb) > 0);
		if (ret) {
			pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
			goto error;
		}
	}

	ret = access_ok(VERIFY_READ, buffer, nbuf);
	if (!ret) {
		pr_err("Invalid user address: %p\n", (void *) buffer);
		goto error;
	} else {
		ret = ringbuffer_write(dev->rb, buffer, nbuf);
		pr_info("Copied %d bytes\n", ret);
	}

	/* Awake any readers */
	wake_up_interruptible(&dev->read_wq);

error:
	mutex_unlock(&dev->write_mtx);

	return ret;
}

/**
 * @page p8
 * @section p8s5 Step 5: Modify the open function
 *
 * In the open function you will control the number of applications that are
 * allowed to open the device. This is done though the use of a semaphore.
 *
 * Note: This is not neccesary, and it is added to this lab only for educational
 * purposes to compare the differences between a mutex and a semaphore. A mutex
 * allows only one thread to have access to the shared resource at a time,
 * while a semaphore allows a limited number of threads to have access to the
 * shared resource simultaneously.
 *
 * For this lab, the number of applications that can open the device
 * simultaneously is defined by `MAX_OPEN_DEVS`. If more than this number
 * attempts to open the device, the process will sleep until one application
 * closes the device and the semaphore lock becomes available. Similarly like
 * you did with the `read` and `write` functions, if the application requested
 * a NONBLOCKING operation and if you are not able to get the semaphore lock,
 * you have to return an error immediately:
 *
 * @code
 * if (filp->f_flags & O_NONBLOCK) {
 *	if (down_trylock(&dev->sem))
 *		return -EAGAIN;
 * } else {
 *	if (down_interruptible(&dev->sem))
 *		return -EINTR;
 * }
 * @endcode
 */


/*
 * Function called when the user side application opens a handle to mydev driver
 * by calling the open() function.
 */
static int mydev_open(struct inode *ip, struct file *filp)
{
	struct my_dev *dev;

	pr_info("[%d] %s\n", __LINE__, __func__);

	/* Get my_dev structure for the device and assign it to private data*/
	dev = container_of(ip->i_cdev, struct my_dev, cdev);

	if (filp->f_flags & O_NONBLOCK) {
		/* NONBLOCK: return immediately if not able to get the lock */
		if (down_trylock(&dev->sem))
			return -EAGAIN;
	} else {
		if (down_interruptible(&dev->sem))
			return -EINTR;
	}

	filp->private_data = dev;

	return 0;
}

/**
 * @page p8
 * @section p8s6 Step 6: Modify the close function
 *
 * In the close function you have to release the semaphore lock by using the
 * `up` function:
 *
 * @code
 *	struct my_dev *dev;
 *	dev = filp->private_data; 
 *	up(&dev->sem);
 * @endcode
 *
 * This will make the semaphore available for another application trying to
 * open the device.
 */


/*
 * Function called when the user side application closes a handle to mydev
 * driver by calling the close() function.
 */
static int mydev_close(struct inode *ip, struct file *filp)
{
	struct my_dev *dev;

	pr_info("[%d] %s\n", __LINE__, __func__);
	dev = filp->private_data; 
	up(&dev->sem);

	return 0;
}

/*
 * mydev_mmap
 * This fuction maps defice memory into user virtual address space.
 */
static int mydev_mmap(struct file *filp, struct vm_area_struct *vma)
{
	unsigned long pfn;
	unsigned long start = (unsigned long)vma->vm_start;
	unsigned long size = (unsigned long)(vma->vm_end - vma->vm_start);
	struct my_dev *dev = filp->private_data;

	pr_info("mmap function was called\n");

	/* If size requested is different than the allocated memory*/
	if (size != PAGE_SIZE) {
		pr_err("Req size = %ld, device buffer size = %ld",
							size, PAGE_SIZE);
		return -EINVAL;
	}

	pfn = (__pa(dev->dev_mem)) >> PAGE_SHIFT;
	if (remap_pfn_range(vma, start, pfn, size, vma->vm_page_prot)) {
		pr_err("Failed to remap");
		return -EAGAIN;
	}

	pr_info("dev_mem: Logical = 0x%p, Physical = 0x%p, pfn = 0x%p",
			dev->dev_mem, (void *)__pa(dev->dev_mem), (void *)pfn);

	return 0;
}

/*
 * mydev_ioctl()
 * This function implements the ioctl function in the file operation structure.
 */
static long mydev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;
	struct my_work_t *work;
	struct data_xfer *xfer = (struct data_xfer *) arg;

	switch(cmd) {
	case MYDEV_START_XFER:	/* Start data transfer */
		work = kmalloc(sizeof(struct my_work_t), GFP_KERNEL);
		if (work) {
			INIT_WORK((struct work_struct *)work, mydev_workq_func);
			work->d_xfer.start = xfer->start;
			work->d_xfer.count = xfer->count;
			work->dev = dev;
			ret = queue_work(dev->workq, (struct work_struct *)work);
			if (!ret) {
				pr_info("Already on the queue\n");
			} else {
				pr_info("Work submitted sucessfully to queue\n");
				ret = 0;	/* Change ret to 0 (success) */
			}
		} else {
			ret = -ENOMEM;
		}
		break;
	case MYDEV_STATUS_XFER:	/* Check the transfer status */
		break;
	case MYDEV_CANCEL_XFER:	/* Cancel data transfer */
		break;
	default:
		return -ENOTTY;	/* Inappropriate ioctl for device */
	}

	return ret;
}

/*
 * File operation structure
 */
static const struct file_operations mydev_fops = {
	.open = mydev_open,
	.release = mydev_close,
	.owner = THIS_MODULE,
	.read = mydev_read,
	.write = mydev_write,
	.mmap = mydev_mmap,
	.unlocked_ioctl = mydev_ioctl,
};

/**
 * @page p8
 * @section p8s7 Step 7: Initialize the Semaphore and Mutexes
 *
 * The semaphore and both mutexes have to be initilized for each device when
 * module is inserted. The place to do it is inside the `mydev_init_device`
 * function:
 *
 * @code
 *	mutex_init(&dev->read_mtx);
 *	mutex_init(&dev->write_mtx);
 *	sema_init(&dev->sem, MAX_OPEN_DEVS);
 * @endcode
 *
 */

/*
 * mydev_init_device()
 * This function allocates memory and initilizes wait queues.
 * returns 0 on success, and a negative error code on failure.
 */
int mydev_init_device(struct my_dev *dev, unsigned int minor)
{
	int ret;

	if (sprintf(dev->name, DEVICE_NAME"%d", minor) < 0) {
		pr_err("Failed to set the device name\n");
		ret = -EINVAL;
		return ret;
	}

	dev->rb = ringbuffer_create(rb_size);
	if (!dev->rb) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to create ring buffer\n", ret);
		return ret;
	}

	dev->dev_mem = (void *)__get_free_page(GFP_KERNEL);
	if (!dev->dev_mem) {
		pr_err("Failed to allocate device memory\n");
		ret = -ENOMEM;
		return ret;
	}
	pr_info("dev_mem%d address = %p\n", minor, dev->dev_mem);

	init_waitqueue_head(&dev->read_wq);
	init_waitqueue_head(&dev->write_wq);
	mutex_init(&dev->read_mtx);
	mutex_init(&dev->write_mtx);
	sema_init(&dev->sem, MAX_OPEN_DEVS);	/* Init semaphore */

	dev->workq = create_workqueue(dev->name);
	if (!dev->workq) {
		pr_err("Failed to create work queue\n");
		ret = -ENOMEM;
		return ret;
	}

	return 0;
}

/*
 * mydev_setup_cdev()
 * This function initializes and adds cdev to the system.
 */
int mydev_setup_cdev(struct my_dev *dev, unsigned int minor)
{
	int err = 0;
	dev_t devid = MKDEV(major, minor);

	cdev_init(&dev->cdev, &mydev_fops);
	dev->cdev.owner = THIS_MODULE;
	err = cdev_add(&dev->cdev, devid, 1);
	if (err)
		pr_err("Error %d adding %s%d", err, DEVICE_NAME, minor);

	return err;
}

/**
 * @page p8
 * @section p8s8 Step 8: Modify the work queue function
 *
 * Since the work queue function also writes to the ring buffer, you have to
 * use a mutex to ensure that this is the only function acessing the ring
 * buffer.
 */

/*
 * mydev_workq_func
 * This is the function to be executed in the work queue.
 */
void mydev_workq_func(struct work_struct *work)
{
	int i;
	int ret = 0;
	struct my_work_t *my_work = (struct my_work_t *) work;
	struct my_dev *dev = my_work->dev;
	int start = my_work->d_xfer.start;
	int count = my_work->d_xfer.count;
	char *buff = dev->dev_mem;

	pr_info("%s: Work queue function was called\n", __func__);
	pr_info("start = %d, count = %d\n", start, count);

	for (i = start; i < start + count; i++) {
		if (ringbuffer_available_space(dev->rb) == 0) {
			pr_info("%s: Going to sleep...\n", __func__);
			ret = wait_event_interruptible(dev->write_wq,
				ringbuffer_available_space(dev->rb) > 0);
			if (ret) {
				pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
				break;
			}
		}
		ret = ringbuffer_write(dev->rb, &buff[i], 1);
		if (ret != 1) {
			pr_err("%s: Failed to write to ringbuffer\n", __func__);
			break;
		}
		msleep(1000);	/* This is introduced to see the transfer */

		/* Awake any readers */
		wake_up_interruptible(&dev->read_wq);
	}

	kfree((void *)work);
}

/*
 * mydev_exit()
 * Module exit function. It is called when rmmod is executed.
 */
static void mydev_exit(void)
{
	int i;
	dev_t devid = MKDEV(major, first_minor);

	/* Deregister char devices from kernel and free memory*/
	if (my_devices) {
		for (i = 0; i < num_devices; i++) {
			cdev_del(&my_devices[i].cdev);
			ringbuffer_destroy(my_devices[i].rb);

			if (my_devices[i].dev_mem) {
				pr_info("dev_mem%d = %s\n", first_minor+i,
						(char *)my_devices[i].dev_mem);
				free_page((unsigned long)my_devices[i].dev_mem);
			}

			flush_workqueue(my_devices[i].workq);
			destroy_workqueue(my_devices[i].workq);
		}
		kfree(my_devices);
	}

	/* Release MAJOR and MINOR numbers */
	unregister_chrdev_region(devid, num_devices);

	/* Deregister device from sysfs */
	if (mydev_class != NULL) {
		for (i = 0; i < num_devices; i++)
			device_destroy(mydev_class, MKDEV(major, first_minor + i));
		class_destroy(mydev_class);
	}

	pr_info("Driver \"%s\" unloaded...\n", DEVICE_NAME);
}

/*
 * mydev_init()
 * Module init function. It is called when insmod is executed.
 */
static int __init mydev_init(void)
{
	int ret = 0;
	int i;
	dev_t devid;

	/* Dynamic allocation of MAJOR and MINOR numbers */
	ret = alloc_chrdev_region(&devid, first_minor, num_devices, DEVICE_NAME);
	if (ret) {
		pr_err("Error %d: Failed registering major number\n", ret);
		return ret;
	}

	/* save MAJOR number */
	major = MAJOR(devid);

	/* Allocate devices */
	my_devices = kzalloc(num_devices * sizeof(struct my_dev), GFP_KERNEL);
	if (!my_devices) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to allocate my_devices\n", ret);
		goto fail;
	}

	/* Initialize devices and add them to the system */
	for (i = 0; i < num_devices; i++) {
		ret = mydev_init_device(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;

		ret = mydev_setup_cdev(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;
	}

	/* Create a class and register with the sysfs. (Failure is not fatal) */
	mydev_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(mydev_class)) {
		pr_info("class_create() failed: %ld\n", PTR_ERR(mydev_class));
		mydev_class = NULL;
		ret = -EFAULT;
		goto fail;
	}
	/* Register device with sysfs (creates device node in /dev) */
	for (i = 0; i < num_devices; i++) {
		mydev_device = device_create(mydev_class, NULL,
						MKDEV(major, first_minor + i),
						NULL, DEVICE_NAME"%d", first_minor+i);
		if (IS_ERR(mydev_device)) {
			pr_info("device_create() failed: %ld\n",
							PTR_ERR(mydev_device));
			mydev_device = NULL;
		}
	}

	pr_info("Driver \"%s\" loaded...\n", DEVICE_NAME);
	return 0;

fail:
	mydev_exit();
	return ret;
}

module_init(mydev_init);
module_exit(mydev_exit);

MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Mutex and Semaphore synchronization methods");

/**
 * @page p8
 * @section p8s9 Summary
 *
 * After following all the steps described, your source code should look similar
 * to this:
 *
 * @include conc_drv.c
 */