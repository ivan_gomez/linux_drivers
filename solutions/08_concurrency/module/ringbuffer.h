/**
 * ringbuffer.h - Ring Buffer
 *
 * Authors: Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 *          Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __RINGBUFFER_H
#define __RINGBUFFER_H

struct ringbuffer {
	char *buff;	/* Ring buffer */
	int size;	/* Buffer size */
	int start;	/* Position to start reading */
	int end;	/* Position to start writing */
	int count;	/* Number of bytes available to read */
};

/* Function prototypes */
struct ringbuffer *ringbuffer_create(unsigned int size);
void ringbuffer_destroy(struct ringbuffer *rb);
int ringbuffer_available_data(struct ringbuffer *rb);
int ringbuffer_available_space(struct ringbuffer *rb);
int ringbuffer_read(struct ringbuffer *rb, char *data, unsigned int num);
int ringbuffer_write(struct ringbuffer *rb, const char *data, unsigned int num);

#endif /* __RINGBUFFER_H */
