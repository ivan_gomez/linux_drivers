/**
 * @file chardrv_ioctl.h - Contains IOCTL command definitions.
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 * @author Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * }
 */

#ifndef __CHARDRV_IOCTL_H
#define __CHARDRV_IOCTL_H

#include <linux/ioctl.h>        /* for IOCTL macros */

#define MYDEV_IOC_MAGIC	'x'

#define MYDEV_START_XFER	_IOW(MYDEV_IOC_MAGIC, 0, int)
#define MYDEV_STATUS_XFER	_IOR(MYDEV_IOC_MAGIC, 1, int)
#define MYDEV_CANCEL_XFER	_IOW(MYDEV_IOC_MAGIC, 2, int)

struct data_xfer {
	int start;	/* start position in the mmapped buffer */
	int count;	/* number of bytes to transfer */
	int status;	/* data transfer status */
};

#endif /* __CHARDRV_IOCTL_H */
