#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#define DEVICE "/dev/mydev"
#define BUFF_SIZE	100
#define NUM_DEVICES	10

int main()
{
	int fd[NUM_DEVICES];
	int i;
	int rc;
	char buff[BUFF_SIZE];
	char dev_name[20];

	/* Write to all devices */
	printf("WRITING...\n");
	for (i = 0; i < NUM_DEVICES; i++) {
		sprintf(dev_name, DEVICE"%d", i);
		fd[i] = open(dev_name, O_RDWR);
		if (fd[i] < 0) {
			printf("Error: Failed to open device %s\n", dev_name);
			return fd[i];
		}

		sprintf(buff, "hola %s", dev_name);
		rc = write(fd[i], buff, strlen(buff));
		if (rc < 0) {
			printf("Error: Failed to write to %s\n", dev_name);
			return rc;
		}
		printf("Writing \"%s\" to %s\n", buff, dev_name);

		rc = close(fd[i]);
		if (rc < 0) {
			printf("Error: Failed to close device %s\n", dev_name);
			return rc;
		}
	}

	/* Read from all devices */
	printf("\nREADING...\n");
	for (i = 0; i < NUM_DEVICES; i++) {
		sprintf(dev_name, DEVICE"%d", i);
		fd[i] = open(dev_name, O_RDWR);
		if (fd[i] < 0) {
			printf("Error: Failed to open device %s\n", dev_name);
			return fd[i];
		}

		rc = read(fd[i], buff, BUFF_SIZE);
		if (rc < 0) {
			printf("Error: Failed to read from %s\n", dev_name);
			return rc;
		}
		printf("Reading from %s: %s\n", dev_name, buff);

		rc = close(fd[i]);
		if (rc < 0) {
			printf("Error: Failed to close device %s\n", dev_name);
			return rc;
		}
	}

	return 0;
}
