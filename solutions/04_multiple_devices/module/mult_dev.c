/**
 * @file mult_dev.c
 * @brief Character driver for multiple devices
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 * @author Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *}
 */

/**
 * @page p4 Lab 04: Multiple devices
 *
 * In this lab you will learn how to write a driver that will control several
 * devices. The devices will be accessed by applications through nodes under
 * `/dev/` file system. The driver will be responsible to create those device
 * nodes and will allocate the resources for each device, such as data buffers.
 */

/**
 * @page p4
 * @section p4s1 Step 1: The module header file
 *
 * Create a header file (mult_dev.h) and add the following code.
 *
 * @code
 * #ifndef __CHAR_DRV_H
 * #define __CHAR_DRV_H
 *
 * #define KBUFF_MAX_SIZE	1024
 * #define DEVICE_NAME	"mydev"
 *
 * struct my_dev {
 *	char *kbuf;
 *	unsigned int kbuf_size;
 *	unsigned int kbuf_len;
 *	struct cdev cdev;
 * };
 *
 * int mydev_setup_cdev(struct my_dev *dev, unsigned int index);
 *
 * #endif
 * @endcode
 */

/**
 * @page p4
 * @section p4s2 Step 2: Add the necessary header files
 *
 * You will have to add the following header files:
 *
 * @code
 * #include <linux/moduleparam.h>
 * #include <linux/kernel.h>
 * #include <linux/slab.h>
 * #include <linux/errno.h>
 * #include <linux/fcntl.h>
 * #include "mult_dev.h"
 * @endcode
 *
 * The header file `linux/moduleparam.h` is needed by module parameters.
 * The header file `linux/kernel.h` contains kernel data types and `printk`
 * function definitions. The header file `linux/slab.h` is used for dynamic
 * memory allocation (`kmalloc` and friends). The header file `linux/errno.h`
 * contains the error codes such as `ENOMEM`, `EFAULT`, etc. The header file
 * `linux/fcntl.h` contains file macros such as `O_ACCMODE` or `O_RDWR`.
 */

#include <linux/module.h>	/* Dynamic loading of modules into the kernel */
#include <linux/fs.h>		/* file defs of important data structures */
#include <linux/cdev.h>		/* char devices structure and aux functions */
#include <linux/device.h>	/* generic, centralized driver model */
#include <linux/uaccess.h>	/* for accessing user-space */
#include <linux/moduleparam.h>	/* Module parameters */
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/errno.h>	/* Error codes */
#include <linux/fcntl.h>	/* O_ACCMODE */
#include "mult_dev.h"

/**
 * @page p4
 * @section p4s3 Step 3:  Declare all the needed global variables and macros
 *
 * Declare a variable to hold the first minor number.
 *
 * @code
 * static unsigned first_minor;
 * @endcode
 *
 * Declare a variable which will contain the total number of devices, and
 * initialize it to a default value you want.
 *
 * @code
 * static unsigned num_devices = 10;
 * @endcode
 *
 * Declare a pointer to `my_dev` structure. This pointer will be used as an
 * array of device structures (we call `my_dev` a device structure, because it
 * contains device specific information).
 *
 * @code
 * struct my_dev *my_devices;
 * @endcode
 */
static unsigned major;			/* major number */
static unsigned first_minor;		/* first minor number */
static unsigned num_devices = 10;	/* number of devices */
static struct class *mydev_class;	/* to register with sysfs */
static struct device *mydev_device;	/* to register with sysfs */

struct my_dev *my_devices;	/* array of structs of num_devices elements */

/**
 * @page p4
 * @section p4s4 Step 4: Define Module parameters
 *
 * There will be two module parameters: `first_minor` and `num_devices`.
 * When loading the module, the user will be able to specify the first minor
 * number to use, by default it is set to zero.
 * The second parameter is the number of devices to be shown in `/dev/`, by
 * default it is set to 10.
 *
 * @code
 * module_param(first_minor, uint, S_IRUGO);
 * module_param(num_devices, uint, S_IRUGO);
 * MODULE_PARM_DESC(first_minor, "First minor number");
 * MODULE_PARM_DESC(num_devices, "Number of devices");
 * @endcode
 */

/* Module parameters */
module_param(first_minor, uint, S_IRUGO);
module_param(num_devices, uint, S_IRUGO);

MODULE_PARM_DESC(first_minor, "First minor number");
MODULE_PARM_DESC(num_devices, "Number of devices");

/**
 * @page p4
 * @section p4s5 Step 5: Modify the read function
 *
 * Instead of using the variable `kbuffer_size`, get the buffer length thorugh
 * the device structure. The `dev->kbuf_len` contains this value, which may be
 * different for each device under `/dev/`.
 *
 * @code
 * struct my_dev *dev = filp->private_data;
 * unsigned int kbuf_len = dev->kbuf_len;
 * @endcode
 *
 * Also you have to replace the kernel buffer variable `kbuffer` by the
 * device-specific buffer `dev->kbuf` when calling the `copy_to_user` function.
 */
/*
 * mydev_read()
 * This function implements the read function in the file operation structure.
 */
static ssize_t mydev_read(struct file *filp, char __user *buffer, size_t nbuf,
								loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;
	unsigned int kbuf_len = dev->kbuf_len;

	pr_info("[%d] %s\n", __LINE__, __func__);

	/* If offset is greater than kbuffer length, there is nothing to copy */
	if (*offset >= kbuf_len) {
		pr_info("Nothing to read: Offset = %d, kbuf_len = %d\n",
						(int)*offset, kbuf_len);
		goto out;
	}

	/* Check for maximum size of data available in kernel buffer */
	if ((nbuf + *offset) > kbuf_len)
		nbuf = kbuf_len - *offset;

	/* fill the buffer, return the number of bytes copied */
	ret = copy_to_user(buffer, &dev->kbuf[*offset], nbuf);
	if (ret) {
		pr_err("[%d] %s copy_to_user failed: %d\n",
						__LINE__, __func__, ret);
		ret = -EFAULT;
		goto out;
	}

	*offset += nbuf;
	ret = nbuf;

out:
	return ret;
}

/**
 * @page p4
 * @section p4s6 Step 6: Modify the write function
 *
 * Instead of using the variable `kbuffer_size`, get the buffer length through
 * the device structure. The `dev->kbuf_len` contains this value, which may be
 * different for each device under `/dev/`.
 * Also you will need to know the maximum buffer size for your specific device,
 * which is `dev->kbuf_size` instead of the `KBUFF_MAX_SIZE` macro.
 * Remember that each device may have a different buffer size, and a different
 * buffer length (i.e. the number of data bytes in the buffer).
 *
 * @code
 * struct my_dev *dev = filp->private_data;
 * unsigned int kbuf_len = dev->kbuf_len;
 * unsigned int kbuf_size = dev->kbuf_size;
 * @endcode
 *
 * Also you have to replace the kernel buffer variable `kbuffer` by the
 * device-specific buffer `dev->kbuf` when calling `copy_from_user`.
 */


/*
 * mydev_write()
 * This function implements the write function in the file operation structure.
 */
static ssize_t mydev_write(struct file *filp, const char __user *buffer,
						size_t nbuf, loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;
	unsigned int kbuf_len = dev->kbuf_len;
	unsigned int kbuf_size = dev->kbuf_size;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (*offset >= kbuf_size) {
		pr_info("No space to write: Offset = %d, kbuf_len = %d\n",
						(int)*offset, kbuf_len);
		goto out;
	}

	/* Check for maximum size of kernel buffer */
	if ((nbuf + *offset) > kbuf_size)
		nbuf = kbuf_size - *offset;

	/* fill the buffer, return the number of byres written */
	ret = copy_from_user(&dev->kbuf[*offset], buffer, nbuf);
	if (ret) {
		pr_err("[%d] %s copy_from_user failed: %d\n",
						__LINE__, __func__, ret);
		ret = -EFAULT;
		goto out;
	}

	*offset += nbuf;
	dev->kbuf_len = *offset;
	ret = nbuf;

out:
	return ret;
}

/**
 * @page p4
 * @section p4s7 Step 7: Modify the open function to set the "private_data"
 *
 * The device structure `my_dev` has to be accessible through the structure
 * `file` in order to let the `read` and `write` functions access the specific
 * device buffer. Otherwise, the `read` and `write` functions wouldn't know
 * which device they are trying to access. You will need to get it through the
 * `inode` structure by using the `container_of` macro:
 *
 * @code
 * container_of - cast a member of a structure out to the containing structure
 * @ptr:        the pointer to the member.
 * @type:       the type of the container struct this is embedded in.
 * @member:     the name of the member within the struct.
 * container_of(ptr, type, member)
 * @endcode
 *
 * In your code, it will look like:
 *
 * @code
 * struct my_dev *dev;
 * dev = container_of(ip->i_cdev, struct my_dev, cdev);
 * filp->private_data = dev;
 * @endcode
 */

/*
 * Function called when the user side application opens a handle to mydev driver
 * by calling the open() function.
 */

static int mydev_open(struct inode *ip, struct file *filp)
{
	struct my_dev *dev;

	pr_info("[%d] %s\n", __LINE__, __func__);
	/* Get my_dev structure for the device and assign it to private data*/
	dev = container_of(ip->i_cdev, struct my_dev, cdev);
	filp->private_data = dev;

	return 0;
}

/*
 * Function called when the user side application closes a handle to mydev
 * driver by calling the close() function.
 */

static int mydev_close(struct inode *ip, struct file *filp)
{
	pr_info("[%d] %s\n", __LINE__, __func__);

	return 0;
}


/*
 * File operation structure
 */
static const struct file_operations mydev_fops = {
	.open = mydev_open,
	.release = mydev_close,
	.owner = THIS_MODULE,
	.read = mydev_read,
	.write = mydev_write
};

/**
 * @page p4
 * @section p4s8 Step 8: Create a function to setup cdev
 *
 * Create a function `mydev_setup_cdev` (its prototype is defined in
 * mult_dev.h). This function will be used to initialize the `cdev` structure
 * and register it with the kernel.
 *
 * @code
 * int mydev_setup_cdev(struct my_dev *dev, unsigned int minor)
 * {
 *	int err = 0;
 *	dev_t devid = MKDEV(major, minor);
 *
 *	cdev_init(&dev->cdev, &mydev_fops);
 *	dev->cdev.owner = THIS_MODULE;
 *	err = cdev_add(&dev->cdev, devid, 1);
 *	if (err)
 *		pr_err("Error %d adding %s%d", err, DEVICE_NAME, minor);
 *
 *	return err;
 * }
 * @endcode
 */

/*
 * mydev_setup_cdev()
 * This function initializes and adds cdev to the system.
 */
int mydev_setup_cdev(struct my_dev *dev, unsigned int minor)
{
	int err = 0;
	dev_t devid = MKDEV(major, minor);

	cdev_init(&dev->cdev, &mydev_fops);
	dev->cdev.owner = THIS_MODULE;
	err = cdev_add(&dev->cdev, devid, 1);
	if (err)
		pr_err("Error %d adding %s%d", err, DEVICE_NAME, minor);

	return err;
}

/**
 * @page p4
 * @section p4s9 Step 9: Modify the exit function
 *
 * The exit function has to be modified in the following way:
 * @par Steps\n
 * @parblock
 * 1 - First you have to remove the `__exit` macro in order to be able to call
 * this function from your code, otherwise, this function will be called only
 * when the module is removed with `rmmod`.
 *
 * 2 - Now that you have multiple devices, you have to deregister each one of
 * those devices (not only one as in the previous lab).
 * @code
 * if (my_devices) {
 *	for (i = 0; i < num_devices; i++) {
 *		cdev_del(&my_devices[i].cdev);
 *		kfree(my_devices[i].kbuf);
 *	}
 *	kfree(my_devices);
 * }
 * @endcode
 *
 * 3 - Release the major and minor numbers, previously you had only one minor
 * number, now you have `num_devices` minor mumbers:
 * @code
 * unregister_chrdev_region(devid, num_devices);
 * @endcode
 *
 * 4 - And finally, remove the nodes created under `/dev/` and `/sys/class/`
 * directories:
 * @code
 * if (mydev_class != NULL) {
 *	for (i = 0; i < num_devices; i++)
 *		device_destroy(mydev_class, MKDEV(major, first_minor + i));
 *	class_destroy(mydev_class);
 * }
 * @endcode
 * @parblockend
 */

/*
 * mydev_exit()
 * Module exit function. It is called when rmmod is executed.
 */
static void mydev_exit(void)
{
	int i;
	dev_t devid = MKDEV(major, first_minor);

	/* Deregister char devices from kernel and free memory*/
	if (my_devices) {
		for (i = 0; i < num_devices; i++) {
			cdev_del(&my_devices[i].cdev);
			kfree(my_devices[i].kbuf);
		}
		kfree(my_devices);
	}

	/* Release MAJOR and MINOR numbers */
	unregister_chrdev_region(devid, num_devices);

	/* Deregister device from sysfs */
	if (mydev_class != NULL) {
		for (i = 0; i < num_devices; i++)
			device_destroy(mydev_class, MKDEV(major, first_minor + i));
		class_destroy(mydev_class);
	}

	pr_info("Driver \"%s\" unloaded...\n", DEVICE_NAME);
}

/**
 * @page p4
 * @section p4s10 Step 10: Modify the init function
 *
 * Since your driver will control multiple devices, the `init` function has to
 * be modified to get the resources needed for all the devices. Follow the next
 * steps to achieve this:
 * @par Steps\n
 * @parblock
 * 1 - Use the `num_devices` variable instead of requesting only one minor
 * number. In addition, you have to use the `first_minor` variable to specify
 * which is the first minor number you are requesting:
 * @code
 * ret = alloc_chrdev_region(&devid, first_minor, num_devices, DEVICE_NAME);
 * @endcode
 * If this fails, return a negative value.
 *
 * 2 - Allocate dynamically the array of devices: If it fails you have to return
 * `-ENOMEM`:
 * @code
 * my_devices = kzalloc(num_devices * sizeof(struct my_dev), GFP_KERNEL);
 * if (!my_devices) {
 *	ret = -ENOMEM;
 *	pr_err("Error %d: Failed to allocate my_devices\n", ret);
 *	goto fail;
 * }
 * @endcode
 *
 * 3 - Now allocate the buffer for each device and initialize the devices by
 * calling the function `mydev_setup_cdev` that you have just created above:
 * @code
 * for (i = 0; i < num_devices; i++) {
 *	my_devices[i].kbuf = kmalloc(KBUFF_MAX_SIZE, GFP_KERNEL);
 *	if (!my_devices[i].kbuf) {
 *		ret = -ENOMEM;
 *		pr_err("Error %d: Failed to allocate kbuff\n", ret);
 *		goto fail;
 *	}
 *	my_devices[i].kbuf_size = KBUFF_MAX_SIZE;
 *	ret = mydev_setup_cdev(&my_devices[i], first_minor + i);
 *	if (ret)
 *		goto fail;
 * }
 * @endcode
 *
 * 4 - You have to create all the device nodes under `/dev/`, that is: `mydev0`,
 * `mydev1`, `mydev2`, etc:
 * @code
 * for (i = 0; i < num_devices; i++) {
 *	mydev_device = device_create(mydev_class, NULL,
 *				MKDEV(major, first_minor + i),
 *				NULL, DEVICE_NAME"%d", first_minor+i);
 *	if (IS_ERR(mydev_device)) {
 *		pr_info("device_create() failed: %ld\n",
 *						PTR_ERR(mydev_device));
 *		mydev_device = NULL;
 *	}
 * }
 * @endcode
 *
 * 5 - Finally, if something went wrong, you have to call the `exit` function to
 * undo everything the `init` function did:
 * @code
 * fail:
 *	mydev_exit();
 *	return ret;
 * @endcode
 * @parblockend
 */

/*
 * mydev__init()
 * Module init function. It is called when insmod is executed.
 */
static int __init mydev_init(void)
{
	int ret = 0;
	int i;
	dev_t devid;

	/* Dynamic allocation of MAJOR and MINOR numbers */
	ret = alloc_chrdev_region(&devid, first_minor, num_devices, DEVICE_NAME);
	if (ret) {
		pr_err("Error %d: Failed registering major number\n", ret);
		return ret;
	}

	/* save MAJOR number */
	major = MAJOR(devid);

	/* Allocate devices */
	my_devices = kzalloc(num_devices * sizeof(struct my_dev), GFP_KERNEL);
	if (!my_devices) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to allocate my_devices\n", ret);
		goto fail;
	}

	/* Initialize devices and add them to the system */
	for (i = 0; i < num_devices; i++) {
		my_devices[i].kbuf = kmalloc(KBUFF_MAX_SIZE, GFP_KERNEL);
		if (!my_devices[i].kbuf) {
			ret = -ENOMEM;
			pr_err("Error %d: Failed to allocate kbuff\n", ret);
			goto fail;
		}
		my_devices[i].kbuf_size = KBUFF_MAX_SIZE;
		ret = mydev_setup_cdev(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;
	}

	/* Create a class and register with the sysfs. (Failure is not fatal) */
	mydev_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(mydev_class)) {
		pr_info("class_create() failed: %ld\n", PTR_ERR(mydev_class));
		mydev_class = NULL;
		ret = -EFAULT;
		goto fail;
	}
	/* Register device with sysfs (creates device node in /dev) */
	for (i = 0; i < num_devices; i++) {
		mydev_device = device_create(mydev_class, NULL,
					MKDEV(major, first_minor + i),
					NULL, DEVICE_NAME"%d", first_minor+i);
		if (IS_ERR(mydev_device)) {
			pr_info("device_create() failed: %ld\n",
							PTR_ERR(mydev_device));
			mydev_device = NULL;
		}
	}

	pr_info("Driver \"%s\" loaded...\n", DEVICE_NAME);
	return 0;

fail:
	mydev_exit();
	return ret;
}

module_init(mydev_init);
module_exit(mydev_exit);

MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("This Char Driver creates multiple devices");

/**
 * @page p4
 * @section p4s11 Summary
 *
 * After following all the steps described, your source code should look similar
 * to this:
 *
 * @include mult_dev.c
 */
