#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#define DEVICE "/dev/mydev0"
#define BUFF_SIZE	1024

int write_to_device(char *dev_name, char *str, int num)
{
	int fd;
	int rc;
	int bytes_copied;

	fd = open(dev_name, O_RDWR | O_NONBLOCK);
	if (fd < 0) {
		printf("Error while opening the device\n");
		return fd;
	}
	printf("Opened: %s\n", dev_name);

	bytes_copied = write(fd, str, num);
	if (bytes_copied < 0) {
		printf("Error while writing to %s\n", dev_name);
		return bytes_copied;
	}
	printf("Wrote: %s\n", str);

	rc = close(fd);
	if (rc < 0) {
		printf("Error while closing %s\n", dev_name);
		return rc;
	}
	printf("Closed: %s\n", dev_name);

	return bytes_copied;
}

int read_from_device(char *dev_name, char *str, int num)
{
	int fd;
	int rc;
	int bytes_copied;

	fd = open(dev_name, O_RDWR | O_NONBLOCK);
	if (fd < 0) {
		printf("Error while opening the device\n");
		return fd;
	}
	printf("Opened: %s\n", dev_name);

	bytes_copied = read(fd, str, num);
	if (bytes_copied < 0) {
		printf("Error while reading from %s\n", dev_name);
		return bytes_copied;
	}
	printf("Read: %s\n", str);

	rc = close(fd);
	if (rc < 0) {
		printf("Error while closing the device\n");
		return rc;
	}
	printf("Closed: %s\n", dev_name);

	return bytes_copied;
}

void pr_help(char *prog_name)
{
	printf("Usage:\n");
	printf("%s <device_name> \"<string to write>\"\n", prog_name);
}

int main(int argc, char *argv[])
{
	int rc;
	char buff[BUFF_SIZE];

	char *device_name;
	char *data;

	if (argc != 3) {
		pr_help(argv[0]);
		return -1;
	}

	device_name = argv[1];
	data = argv[2];

	rc = read_from_device(device_name, buff, BUFF_SIZE);
	if (rc < 0) {
		printf("Failed to read from device\n");
		return -1;
	}

	rc = write_to_device(device_name, data, strlen(data) + 1);
	if (rc < 0) {
		printf("Failed to write to device\n");
		return -1;
	}

	rc = read_from_device(device_name, buff, BUFF_SIZE);
	if (rc < 0) {
		printf("Failed to read from device\n");
		return -1;
	}

	return 0;
}
