/**
 * @file blocking_io.c
 * @brief Character driver implementing blocking IO
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 * @author Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * }
 */

/**
 * @page p5 Lab 05: Blocking I/O
 *
 * In this lab you will learn how to block the application (i.e. put it to
 * sleep) when:
 *
 * 1. The application tries to read data from the device, but the device buffer
 * is empty, or
 *
 * 2. The application tries to write data to the device, but the buffer is full.
 *
 * In these two cases the application will be blocked until there is data to
 * read, or there is available space in the buffer to write, respectively.
 */

/**
 * @page p5
 * @section p5s1 Step 1: The module header file
 *
 * Edit the blocking_io.h file and include the ringbuffer header file:
 * @code
 * #include "ringbuffer.h"
 * @endcode
 *
 * Reduce the buffer size from 1024 to a very small value. This is to fill the
 * buffer quickly, which will cause the write application to be blocked.
 * @code
 * #define KBUFF_SIZE	16
 * @endcode
 *
 * Replace `my_dev` structure by this one:
 * @code
 * struct my_dev {
 *	char name[32];
 *	struct ringbuffer *rb;
 *	struct cdev cdev;
 *	wait_queue_head_t read_wq;
 *	wait_queue_head_t write_wq;
 * };
 * @endcode
 * Now it will hold the device name (i.e. `mydev0`), a pointer to a ring buffer
 * structure which will simulate the device memory, and a read and write wait
 * queues.
 *
 * Finally, add the following function prototype
 * @code
 * int mydev_init_device(struct my_dev *dev, unsigned int minor);
 * @endcode
 * This function will be defined in blocking_io.c and will be used to initialize
 * the devices.
 */

/**
 * @page p5
 * @section p5s2 Step 2: Add the necessary header files
 *
 * Now edit the blocking_io.c file. You will have to add the following header
 * files:
 *
 * @code
 * #include <linux/wait.h>
 * #include <linux/sched.h>
 * #include "blocking_io.h"
 * @endcode
 *
 * The header file `linux/wait.h` contains the wait queue definitions.
 * The header file `linux/sched.h` contains the `TASK_INTERRUPTIBLE` definition
 * needed by the `wait_event_interruptible` function. The header file
 * `blocking_io.h` is the header file you have just edited above.
 */

#include <linux/module.h>	/* Dynamic loading of modules into the kernel */
#include <linux/fs.h>		/* file defs of important data structures */
#include <linux/cdev.h>		/* char devices structure and aux functions */
#include <linux/device.h>	/* generic, centralized driver model */
#include <linux/uaccess.h>	/* for accessing user-space */
#include <linux/moduleparam.h>	/* Module parameters */
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/errno.h>	/* Error codes */
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/wait.h>		/* for wait_event and wake_up */
#include <linux/sched.h>	/* for TASK_INTERRUPTIBLE */
#include "blocking_io.h"

/**
 * @page p5
 * @section p5s3 Step 3: Declare all the needed global variables and macros
 *
 * Declare a variable (which will be a module parameter) to hold the ring buffer
 * size, and set it to `KBUFF_SIZE`:
 *
 * @code
 * static unsigned rb_size = KBUFF_SIZE;
 * @endcode
 */
static unsigned major;			/* major number */
static unsigned first_minor;		/* first minor number */
static unsigned num_devices = 10;	/* number of devices */
static unsigned rb_size = KBUFF_SIZE;	/* ring buffer size */
static struct class *mydev_class;	/* to register with sysfs */
static struct device *mydev_device;	/* to register with sysfs */

struct my_dev *my_devices;	/* array of structs of num_devices elements */

/**
 * @page p5
 * @section p5s4 Step 4: Define Module parameters
 *
 * An additional module parameter will be added: `rb_size`. When inserting the
 * the module, the user will be able to specify the maximum ring buffer size.
 * By default it is set to `KBUFF_SIZE`.
 *
 * @code
 * module_param(rb_size, uint, S_IRUGO);
 * MODULE_PARM_DESC(rb_size, "Ring Buffer size");
 * @endcode
 */
/* Module parameters */
module_param(first_minor, uint, S_IRUGO);
module_param(num_devices, uint, S_IRUGO);
module_param(rb_size, uint, S_IRUGO);

MODULE_PARM_DESC(first_minor, "First minor number");
MODULE_PARM_DESC(num_devices, "Number of devices");
MODULE_PARM_DESC(rb_size, "Ring Buffer size");

/**
 * @page p5
 * @section p5s5 Step 5: Modify the read function
 *
 * The read function needs to be modified for two reasons:
 *
 * 1. To use the ring buffer instead of the `kbuff` buffer.
 *
 * 2. To block the application when there is no available data to read.
 *
 * The algorithm that you have to implement is:
 * 1. Check if there is data available to read in the ring buffer by calling
 * @code
 * ringbuffer_available_data(dev->rb)
 * @endcode
 *
 * 2. If there is no data to read, check if the application requested not to get
 * blocked by setting the `O_NONBLOCK` flag in `file->f_flags`.
 *
 * 3. If `O_NONBLOCK` was requested, return `-EAGAIN` error immediately.
 * @code
 * if (filp->f_flags & O_NONBLOCK) {
 *	pr_info("O_NONBLOCK flag set\n");
 *	return -EAGAIN;
 * }
 * @endcode
 *
 * 4. If not, put the process to sleep in the wait queue `dev->read_wq`, and
 * wait for data to become available in the ring buffer. This wait queue will be
 * awakened by the `write` function when some data is written to the ring
 *  buffer. Take into account that this `wait_event_interruptible` function
 * can be interrupted by any signals (such as killing the application). If this
 * happens, the function will return a non_zero value.
 * @code
 * ret = wait_event_interruptible(dev->read_wq,
 *				ringbuffer_available_data(dev->rb) > 0);
 * if (ret) {
 *	pr_warning("%s: Error(%d): Interrupted by a signal\n", __func__, ret);
 *	return -ERESTARTSYS;
 * }
 * @endcode
 *
 * 5. After waking up, proceed to read the data from the ring buffer. But before
 * writing to the user-space buffer, verify that the address is valid so you can
 * write data to it:
 * @code
 * ret = access_ok(VERIFY_WRITE, buffer, nbuf);
 * if (!ret) {
 *	pr_err("Invalid user address: %p\n", (void *)buffer);
 *	ret = -EFAULT;
 * } else {
 *	ret = ringbuffer_read(dev->rb, buffer, nbuf);
 * }
 * @endcode
 *
 * 6. Finally, wake up the write wait queue, so any blocked application willing
 * to write will be awakened and will be able to write some data. To wake up the
 * wait queue, use:
 * @code
 * wake_up_interruptible(&dev->write_wq);
 * @endcode
 * and return the number of bytes read from the ring buffer:
 * @code
 * return ret;
 * @endcode
 */

/*
 * mydev_read()
 * This function implements the read function in the file operation structure.
 */
static ssize_t mydev_read(struct file *filp, char __user *buffer, size_t nbuf,
								loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (ringbuffer_available_data(dev->rb) == 0) {
		/* If there is nothing to read and O_NONBLOCK is set, return */
		if (filp->f_flags & O_NONBLOCK) {
			pr_info("O_NONBLOCK flag set\n");
			return -EAGAIN;
		}

		/* Go to sleep until data is available */
		/* If interrupted by a signal, return -ERESTARTSYS */
		pr_info("%s: Going to sleep...\n", __func__);
		ret = wait_event_interruptible(dev->read_wq,
					ringbuffer_available_data(dev->rb) > 0);
		if (ret) {
			pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
			return -ERESTARTSYS;
		}
	}

	ret = access_ok(VERIFY_WRITE, buffer, nbuf);
	if (!ret) {
		pr_err("Invalid user address: %p\n", (void *)buffer);
		ret = -EFAULT;
	} else {
		ret = ringbuffer_read(dev->rb, buffer, nbuf);
	}

	/* Awake any writers */
	wake_up_interruptible(&dev->write_wq);

	return ret;
}


/**
 * @page p5
 * @section p5s6 Step 6: Modify the write function
 *
 * The write function has to be modified similarly to the read function.
 * In this case, the algorithm that you have to implement is:
 * 1. Check if there is space available in the ring buffer to write some data.
 * You do this by calling:
 * @code
 * ringbuffer_available_space(dev->rb)
 * @endcode
 *
 * 2. If there is no available space, check if the application requested not to
 * get blocked by setting the `O_NONBLOCK` flag in `file->f_flags`.
 *
 * 3. If `O_NONBLOCK` was requested, return `-EAGAIN` error immediately.
 * @code
 * if (filp->f_flags & O_NONBLOCK) {
 *	pr_info("O_NONBLOCK flag set\n");
 *	return -EAGAIN;
 * }
 * @endcode
 *
 * 4. If not, put the process to sleep in the wait queue `dev->write_wq`, and
 * wait for space to become available in the ring buffer. This wait queue will
 * be awakened by the `read` function when some data is read from the ring
 * buffer. Take into account that the wait function (`wait_event_interruptible`)
 * can be interrupted by any signals (such as killing the application). If this
 * happens, the function will return a non_zero value.
 * @code
 * ret = wait_event_interruptible(dev->write_wq,
 *				ringbuffer_available_space(dev->rb) > 0);
 * if (ret) {
 *	pr_warning("%s: Error(%d): Interrupted by a signal\n", __func__, ret);
 *	return -ERESTARTSYS;
 * }
 * @endcode
 *
 * 5. After waking up, proceed to write the data to the ring buffer. But before
 * reading from the user-space buffer, verify that the address is valid and you
 * can read data from it:
 * @code
 * ret = access_ok(VERIFY_READ, buffer, nbuf);
 * if (!ret) {
 *	pr_err("Invalid user address: %p\n", (void *)buffer);
 *	ret = -EFAULT;
 * } else {
 *	ret = ringbuffer_write(dev->rb, buffer, nbuf);
 * }
 * @endcode
 *
 * 6. Finally, wake up the read wait queue, so any blocked application willing
 * to read will be awakened and will be able to read data from the ring bufer.
 * To wake up the wait queue, use:
 * @code
 * wake_up_interruptible(&dev->read_wq);
 * @endcode
 * and return the number of bytes written to the ring buffer
 * @code
 * return ret;
 * @endcode
 */

/*
 * mydev_write()
 * This function implements the write function in the file operation structure.
 */
static ssize_t mydev_write(struct file *filp, const char __user *buffer,
						size_t nbuf, loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (ringbuffer_available_space(dev->rb) == 0) {
		/* If the ring buffer is full and O_NONBLOCK is set, return */
		if (filp->f_flags & O_NONBLOCK) {
			pr_info("O_NONBLOCK flag set\n");
			return -EAGAIN;
		}

		/* Go to sleep until there is space in the ring buffer */
		/* If interrupted by a signal, return -ERESTARTSYS */
		pr_info("%s: Going to sleep...\n", __func__);
		ret = wait_event_interruptible(dev->write_wq,
				ringbuffer_available_space(dev->rb) > 0);
		if (ret) {
			pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
			return -ERESTARTSYS;
		}
	}

	ret = access_ok(VERIFY_READ, buffer, nbuf);
	if (!ret) {
		pr_err("Invalid user address: %p\n", (void *) buffer);
		ret = -EFAULT;
	} else {
		ret = ringbuffer_write(dev->rb, buffer, nbuf);
		pr_info("Copied %d bytes\n", ret);
	}

	/* Awake any readers */
	wake_up_interruptible(&dev->read_wq);

	return ret;
}

/*
 * Function called when the user side application opens a handle to mydev driver
 * by calling the open() function.
 */

static int mydev_open(struct inode *ip, struct file *filp)
{
	struct my_dev *dev;

	pr_info("[%d] %s\n", __LINE__, __func__);
	/* Get my_dev structure for the device and assign it to private data*/
	dev = container_of(ip->i_cdev, struct my_dev, cdev);
	filp->private_data = dev;

	return 0;
}

/*
 * Function called when the user side application closes a handle to mydev
 * driver by calling the close() function.
 */

static int mydev_close(struct inode *ip, struct file *filp)
{
	pr_info("[%d] %s\n", __LINE__, __func__);

	return 0;
}


/*
 * File operation structure
 */
static const struct file_operations mydev_fops = {
	.open = mydev_open,
	.release = mydev_close,
	.owner = THIS_MODULE,
	.read = mydev_read,
	.write = mydev_write
};

/**
 * @page p5
 * @section p5s7 Step 7: Create a function to initialize the devices
 *
 * Create the function `mydev_init_device` (prototype defined in blocking_io.h).
 * This function will be used to initialize the devices: set the device name,
 * allocate memory for the ring buffer, and initilaize the wait queues.
 *
 * 1. Set the device name:
 * @code
 * if (sprintf(dev->name, DEVICE_NAME"%d", minor) < 0) {
 *	pr_err("Failed to set the device name\n");
 *	ret = -EINVAL;
 *	return ret;
 * }
 * @endcode
 *
 * 2. Create the ring buffer for the device:
 * @code
 * dev->rb = ringbuffer_create(rb_size);
 * if (!dev->rb) {
 *	ret = -ENOMEM;
 *	pr_err("Error %d: Failed to create ring buffer\n", ret);
 *	return ret;
 * }
 * @endcode
 *
 * 3. Initialize the read and write wait queues:
 * @code
 * init_waitqueue_head(&dev->read_wq);
 * init_waitqueue_head(&dev->write_wq);
 * @endcode
 *
 * 4. If everything went well, return zero. Otherwise, return a negative value.
 */

/*
 * mydev_init_device()
 * This function allocates memory and initilizes wait queues.
 * returns 0 on success, and a negative error code on failure.
 */
int mydev_init_device(struct my_dev *dev, unsigned int minor)
{
	int ret;

	if (sprintf(dev->name, DEVICE_NAME"%d", minor) < 0) {
		pr_err("Failed to set the device name\n");
		ret = -EINVAL;
		return ret;
	}

	dev->rb = ringbuffer_create(rb_size);
	if (!dev->rb) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to create ring buffer\n", ret);
		return ret;
	}

	init_waitqueue_head(&dev->read_wq);
	init_waitqueue_head(&dev->write_wq);

	pr_info("device%d initializated successfully\n", minor);

	return 0;
}

/*
 * mydev_setup_cdev()
 * This function initializes and adds cdev to the system.
 */
int mydev_setup_cdev(struct my_dev *dev, unsigned int minor)
{
	int err = 0;
	dev_t devid = MKDEV(major, minor);

	cdev_init(&dev->cdev, &mydev_fops);
	dev->cdev.owner = THIS_MODULE;
	err = cdev_add(&dev->cdev, devid, 1);
	if (err)
		pr_err("Error %d adding %s%d", err, DEVICE_NAME, minor);

	return err;
}

/**
 * @page p5
 * @section p5s8 Step 8: Modify the exit function
 *
 * Since the `kbuf` buffer was replaced by the ring buffer, you will have to
 * replace the line
 * @code
 * kfree(my_devices[i].kbuf);
 * @endcode
 *
 * by
 * @code
 * ringbuffer_destroy(my_devices[i].rb);
 * @endcode
 * which will destroy the ring buffer by freeing the buffer and the structure.
 * See ringbuffer.c file for details.
 * */

/*
 * mydev_exit()
 * Module exit function. It is called when rmmod is executed.
 */
static void mydev_exit(void)
{
	int i;
	dev_t devid = MKDEV(major, first_minor);

	/* Deregister char devices from kernel and free memory*/
	if (my_devices) {
		for (i = 0; i < num_devices; i++) {
			cdev_del(&my_devices[i].cdev);
			ringbuffer_destroy(my_devices[i].rb);
		}
		kfree(my_devices);
	}

	/* Release MAJOR and MINOR numbers */
	unregister_chrdev_region(devid, num_devices);

	/* Deregister device from sysfs */
	if (mydev_class != NULL) {
		for (i = 0; i < num_devices; i++)
			device_destroy(mydev_class, MKDEV(major, first_minor + i));
		class_destroy(mydev_class);
	}

	pr_info("Driver \"%s\" unloaded...\n", DEVICE_NAME);
}

/**
 * @page p5
 * @section p5s9 Step 9: Modify the init function
 *
 * Instead of allocating memory for `my_devices[i].kbuf` and setting the
 * `my_devices[i].kbuf_size`, you only need to call `mydev_init_device`, which
 * will take care of all of that:
 * @code
 * ret = mydev_init_device(&my_devices[i], first_minor + i);
 * if (ret)
 *	goto fail;
 * @endcode
 *
 * And since you have set up the device name, you can replace the line:
 * @code
 * mydev_device = device_create(mydev_class, NULL,
 *				MKDEV(major, first_minor + i),
 *				NULL, DEVICE_NAME"%d", first_minor+i);
 * @endcode
 *
 * by
 * @code
 * mydev_device = device_create(mydev_class, NULL,
 *				MKDEV(major, first_minor + i),
 *				NULL, my_devices[i].name);
 * @endcode
 * */

/*
 * mydev__init()
 * Module init function. It is called when insmod is executed.
 */
static int __init mydev_init(void)
{
	int ret = 0;
	int i;
	dev_t devid;

	/* Dynamic allocation of MAJOR and MINOR numbers */
	ret = alloc_chrdev_region(&devid, first_minor, num_devices, DEVICE_NAME);
	if (ret) {
		pr_err("Error %d: Failed registering major number\n", ret);
		return ret;
	}

	/* save MAJOR number */
	major = MAJOR(devid);

	/* Allocate devices */
	my_devices = kzalloc(num_devices * sizeof(struct my_dev), GFP_KERNEL);
	if (!my_devices) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to allocate my_devices\n", ret);
		goto fail;
	}

	/* Initialize devices and add them to the system */
	for (i = 0; i < num_devices; i++) {
		ret = mydev_init_device(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;

		ret = mydev_setup_cdev(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;
	}

	/* Create a class and register with the sysfs. (Failure is not fatal) */
	mydev_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(mydev_class)) {
		pr_info("class_create() failed: %ld\n", PTR_ERR(mydev_class));
		mydev_class = NULL;
		ret = -EFAULT;
		goto fail;
	}
	/* Register device with sysfs (creates device node in /dev) */
	for (i = 0; i < num_devices; i++) {
		mydev_device = device_create(mydev_class, NULL,
						MKDEV(major, first_minor + i),
						NULL, my_devices[i].name);
		if (IS_ERR(mydev_device)) {
			pr_info("device_create() failed: %ld\n",
							PTR_ERR(mydev_device));
			mydev_device = NULL;
		}
	}

	pr_info("Driver \"%s\" loaded...\n", DEVICE_NAME);
	return 0;

fail:
	mydev_exit();
	return ret;
}

module_init(mydev_init);
module_exit(mydev_exit);

MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("This Char Driver demostrates the use of Blocking IO");

/**
 * @page p5
 * @section p5s10 Step 10: How to test this module
 *
 * 1. Open three terminals.
 * 2. In the first terminal keep reading the kernel log messages:
 * @code
 * tail -f /var/log/kern.log
 * @endcode
 *
 * 2. In the second terminal keep writing some strings to the device (as super
 * user, or change the permissions of the device node) until the buffer is
 * filled up:
 * @code
 * echo "Hello..." > /dev/mydev0
 * @endcode
 *
 * 3. At this time, `echo` will be blocked. To unblock it, you have to read some
 * data from the device:
 * @code
 * cat /dev/mydev0
 * @endcode
 *
 * 4. Now the read function got blocked. To veryfy that `cat` is sleeping, check
 * the process status by typing:
 * @code
 * top -p <cat_pid>
 * @endcode
 *
 * If you write some more data, `cat` will be awakened and will read the data
 * you have just written to the device.
 */

/**
 * @page p5
 * @section p5s11 Summary
 *
 * After following all the steps described, your source code should look similar
 * to this:
 *
 * @include blocking_io.c
 */
