/**
 * @file blocking_io.h - Character driver implementing blocking IO
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 * @author Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * }
 */

#ifndef __CHAR_DRV_H
#define __CHAR_DRV_H

#include "ringbuffer.h"

#define KBUFF_SIZE	16	/* size of the kernel side buffer */
#define DEVICE_NAME	"mydev"

struct my_dev {
	char name[32];			/* device name */
	struct ringbuffer *rb;		/* Data buffer */
	struct cdev cdev;		/* cdev */
	wait_queue_head_t read_wq;	/* read wait queue */
	wait_queue_head_t write_wq;	/* write wait queue */
};

/* Function prototypes */
int mydev_init_device(struct my_dev *dev, unsigned int minor);
int mydev_setup_cdev(struct my_dev *dev, unsigned int minor);

#endif /* __CHAR_DRV_H */
