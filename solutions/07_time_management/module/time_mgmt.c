/**
 * @file time_mgmt.c
 * @brief Deferred execution through work queues.
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * }
 */

/**
 * @page p7 Lab 07: Time Management
 *
 * In this lab you will learn how to defer the execution of a function. Also,
 * you will learn how to use and implement a new system call: The `ioctl`
 * function.
 *
 * To achieve the above objectives, you will have use both buffers created in
 * previous labs:
 * 1. the ring buffer, and
 * 2. the memory mapped buffer.
 *
 * The application, located in `labs/07_time_management/app/`, writes data
 * directly into the memory mapped buffer. After that, it requests to transfer
 * blocks of data from the mapped buffer to the ring buffer. This is done by
 * using the `ioctl` system call.
 *
 * Please note that the aplication only sends a command to request the transfer
 * but the actual data transfer is scheduled to be done later (i.e. The device
 * driver defers the execution of the function in charge to carry out the data
 * transfer). This way the application does not wait for the data transfer to
 * complete. Additionally, the application can request multiple data transfers
 * and the driver will place those request in a queue: The workqueue.
 */

/**
 * @page p7
 * @section p7s1 Step 1: The module header files
 *
 * Create a header named chardrv_ioctl.h. This header file will contain the
 * IOCTL commands supported by this driver. Edit the file and add the following:
 *
 * @code
 * #ifndef __CHARDRV_IOCTL_H
 * #define __CHARDRV_IOCTL_H
 *
 * #include <linux/ioctl.h>
 *
 * #define MYDEV_IOC_MAGIC 'x'
 *
 * #define MYDEV_START_XFER        _IOW(MYDEV_IOC_MAGIC, 0, int)
 * #define MYDEV_STATUS_XFER       _IOR(MYDEV_IOC_MAGIC, 1, int)
 * #define MYDEV_CANCEL_XFER       _IOW(MYDEV_IOC_MAGIC, 2, int)
 *
 * struct data_xfer {
 *         int start;
 *         int count;
 *         int status;
 * };
 *
 * #endif
 * @endcode
 *
 * Now edit the header file time_mgmt.h and include:
 * @code
 * #include <linux/workqueue.h>
 * #include "chardrv_ioctl.h"
 * @endcode
 *
 * In the structure `my_dev`, add a new field to hold the work queue:
 * @code
 * struct workqueue_struct *workq;
 * @endcode
 *
 * Now create a new structure `my_work_t`.
 * @code
 * struct my_work_t {
 *	struct work_struct my_work;
 *	struct data_xfer d_xfer;
 *	struct my_dev *dev;
 * };
 * @endcode
 * This will be used as the argument passed to each function in the work queue.
 * Imagine that the work queue is a queue of function pointers and each of these
 * functions receives a pointer to `my_work_t` structure as argument.
 *
 * Finally, add the prototype of the work queue function that will be deferred.
 * This function will be the responsible to carry out the data transfer
 * requested by the application.
 * @code
 * void mydev_workq_func(struct work_struct *work);
 * @endcode
 * Please note that this function's argument is a pointer to `work_struct`
 * instead of a pointer to `my_work_t`. This is required by the work queue
 * interface. We created the structure `my_work_t` as a trick to get access
 * to the information we want inside the function `mydev_workq_func`, by simply
 * casting the pointer received as parameter to a pointer to `my_work_t` (note
 * that the first field of `my_work_t` is a pointer to `work_struct`).
 */

/**
 * @page p7
 * @section p7s2 Step 2: Add the necessary header files
 *
 * Now edit the time_mgmt.c file. You will have to add the following header
 * files:
 *
 * @code
 * #include <linux/delay.h>
 * #include "chardrv_ioctl.h"
 * #include "time_mgmt.h"
 * @endcode
 *
 * The header file `linux/delay.h` contains functions to delay execution, such
 * as the `msleep` function.
 * The header file `chardrv_ioctl.h` is the file you created above, which
 * contains the IOCTL commands supported by this module.
 * The header file `time_mgmt.h` is the header file you have just modified above.
 */

#include <linux/module.h>	/* Dynamic loading of modules into the kernel */
#include <linux/fs.h>		/* file defs of important data structures */
#include <linux/cdev.h>		/* char devices structure and aux functions */
#include <linux/device.h>	/* generic, centralized driver model */
#include <linux/uaccess.h>	/* for accessing user-space */
#include <linux/moduleparam.h>	/* Module parameters */
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/errno.h>	/* Error codes */
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/wait.h>		/* for wait_event and wake_up */
#include <linux/sched.h>	/* for TASK_INTERRUPTIBLE */
#include <linux/mm.h>		/* for memory mapping (mmap) */
#include <linux/delay.h>	/* for msleep */
#include "chardrv_ioctl.h"	/* IOCTL commands */
#include "time_mgmt.h"

static unsigned major;			/* major number */
static unsigned first_minor;		/* first minor number */
static unsigned num_devices = 10;	/* number of devices */
static unsigned rb_size = KBUFF_SIZE;	/* ring buffer size */
static struct class *mydev_class;	/* to register with sysfs */
static struct device *mydev_device;	/* to register with sysfs */

struct my_dev *my_devices;	/* array of structs of num_devices elements */

/* Module parameters */
module_param(first_minor, uint, S_IRUGO);
module_param(num_devices, uint, S_IRUGO);
module_param(rb_size, uint, S_IRUGO);

MODULE_PARM_DESC(first_minor, "First minor number");
MODULE_PARM_DESC(num_devices, "Number of devices");
MODULE_PARM_DESC(rb_size, "Ring Buffer size");

/*
 * mydev_read()
 * This function implements the read function in the file operation structure.
 */
static ssize_t mydev_read(struct file *filp, char __user *buffer, size_t nbuf,
								loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (ringbuffer_available_data(dev->rb) == 0) {
		/* If there is nothing to read and O_NONBLOCK is set, return */
		if (filp->f_flags & O_NONBLOCK) {
			pr_info("O_NONBLOCK flag set\n");
			return -EAGAIN;
		}

		/* Go to sleep until data is available */
		/* If interrupted by a signal, return -ERESTARTSYS */
		pr_info("%s: Going to sleep...\n", __func__);
		ret = wait_event_interruptible(dev->read_wq,
					ringbuffer_available_data(dev->rb) > 0);
		if (ret) {
			pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
			return -ERESTARTSYS;
		}
	}

	ret = access_ok(VERIFY_WRITE, buffer, nbuf);
	if (!ret) {
		pr_err("Invalid user address: %p\n", (void *)buffer);
		ret = -EFAULT;
	} else {
		ret = ringbuffer_read(dev->rb, buffer, nbuf);
	}

	/* Awake any writers */
	wake_up_interruptible(&dev->write_wq);

	return ret;
}

/*
 * mydev_write()
 * This function implements the write function in the file operation structure.
 */
static ssize_t mydev_write(struct file *filp, const char __user *buffer,
						size_t nbuf, loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (ringbuffer_available_space(dev->rb) == 0) {
		/* If the ring buffer is full and O_NONBLOCK is set, return */
		if (filp->f_flags & O_NONBLOCK) {
			pr_info("O_NONBLOCK flag set\n");
			return -EAGAIN;
		}

		/* Go to sleep until there is space in the ring buffer */
		/* If interrupted by a signal, return -ERESTARTSYS */
		pr_info("%s: Going to sleep...\n", __func__);
		ret = wait_event_interruptible(dev->write_wq,
				ringbuffer_available_space(dev->rb) > 0);
		if (ret) {
			pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
			return -ERESTARTSYS;
		}
	}

	ret = access_ok(VERIFY_READ, buffer, nbuf);
	if (!ret) {
		pr_err("Invalid user address: %p\n", (void *) buffer);
		ret = -EFAULT;
	} else {
		ret = ringbuffer_write(dev->rb, buffer, nbuf);
		pr_info("Copied %d bytes\n", ret);
	}

	/* Awake any readers */
	wake_up_interruptible(&dev->read_wq);

	return ret;
}

/*
 * Function called when the user side application opens a handle to mydev driver
 * by calling the open() function.
 */

static int mydev_open(struct inode *ip, struct file *filp)
{
	struct my_dev *dev;

	pr_info("[%d] %s\n", __LINE__, __func__);
	/* Get my_dev structure for the device and assign it to private data*/
	dev = container_of(ip->i_cdev, struct my_dev, cdev);
	filp->private_data = dev;

	return 0;
}

/*
 * Function called when the user side application closes a handle to mydev
 * driver by calling the close() function.
 */

static int mydev_close(struct inode *ip, struct file *filp)
{
	pr_info("[%d] %s\n", __LINE__, __func__);

	return 0;
}

/*
 * mydev_mmap
 * This fuction maps defice memory into user virtual address space.
 */
static int mydev_mmap(struct file *filp, struct vm_area_struct *vma)
{
	unsigned long pfn;
	unsigned long start = (unsigned long)vma->vm_start;
	unsigned long size = (unsigned long)(vma->vm_end - vma->vm_start);
	struct my_dev *dev = filp->private_data;

	pr_info("mmap function was called\n");

	/* If size requested is different than the allocated memory*/
	if (size != PAGE_SIZE) {
		pr_err("Req size = %ld, device buffer size = %ld",
							size, PAGE_SIZE);
		return -EINVAL;
	}

	pfn = (__pa(dev->dev_mem)) >> PAGE_SHIFT;
	if (remap_pfn_range(vma, start, pfn, size, vma->vm_page_prot)) {
		pr_err("Failed to remap");
		return -EAGAIN;
	}

	pr_info("dev_mem: Logical = 0x%p, Physical = 0x%p, pfn = 0x%p",
			dev->dev_mem, (void *)__pa(dev->dev_mem), (void *)pfn);

	return 0;
}

/**
 * @page p7
 * @section p7s3 Step 3: Create the ioctl function
 *
 * The IOCTL function is used by the application to send commands to the device.
 * Each command is an integer number, defined by you in the `chardrv_ioctl.h`
 * header file.
 *
 * In user-space, the `ioctl` receives two parameters: The file descriptor and
 * the command number. A third parameter is optional, and usually it's a pointer
 * to a structure.
 *
 * In kernel-space, the `ioctl` function has the following prototype:
 *
 * @code
 * static long mydev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);
 * @endcode
 *
 * The parameter `filp` is a pointer to the `file` structure, the same used in
 * the `read` and `write` functions. The parameter `cmd` is the command number
 * sent by the user-space application, and the parameter `arg` is the optional
 * argument passed by the user-space application.
 *
 * Although there are three commands defined in `chardrv_ioctl.h`, you will only
 * support one: `MYDEV_START_XFER`.
 *
 * The `ioctl` function is implemented with a `switch` statement. When the
 * command received is `MYDEV_START_XFER`, you will have to do the following:
 *
 * 1. Check that the user space pointer pased as argument is valid. This is done
 * with the macro `access_ok`. It returns zero if the address is valid. If the
 * address is invalid, return an error immediately.
 * DON'T TRUST USER SPACE INPUT, ALWAYS CHECK PARAMETERS RECEIVED.
 *
 * 2. Allocate memory for the structure `my_work_t` to be passed as argument to
 * the deferred function. If it fails to allocate memory, return `-ENOMEM`.
 *
 * 3. Initialize the structure `work_struct` with `INIT_WORK`.
 *
 * 4. Initialize the structure `my_work_t` with the values passed by the user
 * space application through the `arg` parameter. The values you have to set are
 * the start position in the mapped bufer, and the number of bytes to copy.
 *
 * 5. Enqueue the work in the workqueue by using the function `queue_work`. If
 * this function returns `false`, it means that the work was already on
 * the queue, if it returns `true`, it means that the work was sucessfully
 * submitted to the workqueue.
 *
 * 6. If a command different than `MYDEV_START_XFER` was received then return
 * -ENOTTY, which means: Inappropriate ioctl for device.
 *
 * After following these steps, your `ioctl` function will look like:
 * @code
 * static long mydev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
 * {
 *	int ret = 0;
 *	struct my_dev *dev = filp->private_data;
 *	struct my_work_t *work;
 *	struct data_xfer *xfer = (struct data_xfer *) arg;
 *
 *	switch(cmd) {
 *	case MYDEV_START_XFER:
 *		ret = access_ok(VERIFY_READ, arg, sizeof(struct data_xfer));
 *		if (!ret) {
 *			pr_err("Invalid user address: %p\n", (void *)arg);
 *			ret = -EFAULT;
 *			break;
 *		}
 *
 *		work = kmalloc(sizeof(struct my_work_t), GFP_KERNEL);
 *		if (work) {
 *			INIT_WORK((struct work_struct *)work, mydev_workq_func);
 *			work->d_xfer.start = xfer->start;
 *			work->d_xfer.count = xfer->count;
 *			work->dev = dev;
 *			ret = queue_work(dev->workq, (struct work_struct *)work);
 *			if (!ret) {
 *				pr_info("Already on the queue\n");
 *			} else {
 *				pr_info("Work submitted sucessfully to queue\n");
 *				ret = 0;
 *			}
 *		} else {
 *			ret = -ENOMEM;
 *		}
 *		break;
 *	default:
 *		return -ENOTTY;
 *	}
 *
 *	return ret;
 * }
 * @endcode
 */

/*
 * mydev_ioctl()
 * This function implements the ioctl function in the file operation structure.
 */
static long mydev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;
	struct my_work_t *work;
	struct data_xfer *xfer = (struct data_xfer *) arg;

	switch(cmd) {
	case MYDEV_START_XFER:	/* Start data transfer */
		ret = access_ok(VERIFY_READ, arg, sizeof(struct data_xfer));
		if (!ret) {
			pr_err("Invalid user address: %p\n", (void *)arg);
			ret = -EFAULT;
			break;
		}

		work = kmalloc(sizeof(struct my_work_t), GFP_KERNEL);
		if (work) {
			INIT_WORK((struct work_struct *)work, mydev_workq_func);
			work->d_xfer.start = xfer->start;
			work->d_xfer.count = xfer->count;
			work->dev = dev;
			ret = queue_work(dev->workq, (struct work_struct *)work);
			if (!ret) {
				pr_info("Already on the queue\n");
			} else {
				pr_info("Work submitted sucessfully to queue\n");
				ret = 0;	/* Change ret to 0 (success) */
			}
		} else {
			ret = -ENOMEM;
		}
		break;
	case MYDEV_STATUS_XFER:	/* Check the transfer status */
		break;
	case MYDEV_CANCEL_XFER:	/* Cancel data transfer */
		break;
	default:
		return -ENOTTY;	/* Inappropriate ioctl for device */
	}

	return ret;
}

/**
 * @page p7
 * @section p7s4 Step 4: Set the unlocked_ioctl field in file_operations
 *
 * You have to set the `unlocked_ioctl` field in the `file_operations` structure
 * to the IOCTL function you have just created in the previous step, just like
 * you did with the `read` and `write` functions. Now your `file_operations`
 * structure will look like:
 *
 * @code
 * static const struct file_operations mydev_fops = {
 *	.open = mydev_open,
 *	.release = mydev_close,
 *	.owner = THIS_MODULE,
 *	.read = mydev_read,
 *	.write = mydev_write,
 *	.mmap = mydev_mmap,
 *	.unlocked_ioctl = mydev_ioctl,
 * };
 * @endcode
 *
 */

/*
 * File operation structure
 */
static const struct file_operations mydev_fops = {
	.open = mydev_open,
	.release = mydev_close,
	.owner = THIS_MODULE,
	.read = mydev_read,
	.write = mydev_write,
	.mmap = mydev_mmap,
	.unlocked_ioctl = mydev_ioctl,
};

/**
 * @page p7
 * @section p7s5 Step 5: Create the work queues
 *
 * At the end of the function `mydev_init_device` you have to create a workqueue
 * for each device. This is done with the `create_workqueue` function. If it
 * returns `NULL` it means that it failed to create the work queue. If this
 * happens, return the `-ENOMEM` error.
 *
 * @code
 * dev->workq = create_workqueue(dev->name);
 * if (!dev->workq) {
 *	pr_err("Failed to create work queue\n");
 *	ret = -ENOMEM;
 *	return ret;
 * }
 * @endcode
 */


/*
 * mydev_init_device()
 * This function allocates memory and initilizes wait queues.
 * returns 0 on success, and a negative error code on failure.
 */
int mydev_init_device(struct my_dev *dev, unsigned int minor)
{
	int ret;

	if (sprintf(dev->name, DEVICE_NAME"%d", minor) < 0) {
		pr_err("Failed to set the device name\n");
		ret = -EINVAL;
		return ret;
	}

	dev->rb = ringbuffer_create(rb_size);
	if (!dev->rb) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to create ring buffer\n", ret);
		return ret;
	}

	dev->dev_mem = (void *)__get_free_page(GFP_KERNEL);
	if (!dev->dev_mem) {
		pr_err("Failed to allocate device memory\n");
		ret = -ENOMEM;
		return ret;
	}
	pr_info("dev_mem%d address = %p\n", minor, dev->dev_mem);

	init_waitqueue_head(&dev->read_wq);
	init_waitqueue_head(&dev->write_wq);

	dev->workq = create_workqueue(dev->name);
	if (!dev->workq) {
		pr_err("Failed to create work queue\n");
		ret = -ENOMEM;
		return ret;
	}

	return 0;
}

/*
 * mydev_setup_cdev()
 * This function initializes and adds cdev to the system.
 */
int mydev_setup_cdev(struct my_dev *dev, unsigned int minor)
{
	int err = 0;
	dev_t devid = MKDEV(major, minor);

	cdev_init(&dev->cdev, &mydev_fops);
	dev->cdev.owner = THIS_MODULE;
	err = cdev_add(&dev->cdev, devid, 1);
	if (err)
		pr_err("Error %d adding %s%d", err, DEVICE_NAME, minor);

	return err;
}

/**
 * @page p7
 * @section p7s6 Step 6: Create the workqueue function
 *
 * This is the function that will be scheduled to complete the data transfer.
 *
 * Important note: The following function was intentionally written in this way
 * for educational purposes to show that:
 *
 * 1. The workqueue can sleep.
 *
 * 2. The application finishes before the data transfer is completed. This can
 * be seen by reading the kernel logs: `tail -f /var/log/kern.log` at the same
 * time the application is running. The driver copies the data at a rate of 1
 * byte per second.
 *
 * This function is very simple, it copies `count` bytes from the mapped buffer
 * to the device ring buffer, 1 byte at a time, begining at the position `start`
 * of the mapped buffer. If the ring buffer is full, this function goes to sleep
 * until some space becomes available. This works the same way as the Blocking
 * I/O lab does.
 * At the end of this function we have to free the memory allocated in the IOCTL
 * function for the structure `work_struct`.
 *
 * @code
 * void mydev_workq_func(struct work_struct *work)
 * {
 *	int i;
 *	int ret = 0;
 *	struct my_work_t *my_work = (struct my_work_t *) work;
 *	struct my_dev *dev = my_work->dev;
 *	int start = my_work->d_xfer.start;
 *	int count = my_work->d_xfer.count;
 *	char *buff = dev->dev_mem;
 *
 *	pr_info("%s: Work queue function was called\n", __func__);
 *	pr_info("start = %d, count = %d\n", start, count);
 *
 *	for (i = start; i < start + count; i++) {
 *		if (ringbuffer_available_space(dev->rb) == 0) {
 *			pr_info("%s: Going to sleep...\n", __func__);
 *			ret = wait_event_interruptible(dev->write_wq,
 *				ringbuffer_available_space(dev->rb) > 0);
 *			if (ret) {
 *				pr_warning("%s: Error(%d): Interrupted by a signal\n",
 *								__func__, ret);
 *				break;
 *			}
 *		}
 *		ret = ringbuffer_write(dev->rb, &buff[i], 1);
 *		if (ret != 1) {
 *			pr_err("%s: Failed to write to ringbuffer\n", __func__);
 *			break;
 *		}
 *		msleep(1000);
 *
 *		wake_up_interruptible(&dev->read_wq);
 *	}
 *
 *	kfree((void *)work);
 * }
 * @endcode
 */

/*
 * mydev_workq_func
 * This is the function to be executed in the work queue.
 */
void mydev_workq_func(struct work_struct *work)
{
	int i;
	int ret = 0;
	struct my_work_t *my_work = (struct my_work_t *) work;
	struct my_dev *dev = my_work->dev;
	int start = my_work->d_xfer.start;
	int count = my_work->d_xfer.count;
	char *buff = dev->dev_mem;

	pr_info("%s: Work queue function was called\n", __func__);
	pr_info("start = %d, count = %d\n", start, count);

	for (i = start; i < start + count; i++) {
		if (ringbuffer_available_space(dev->rb) == 0) {
			pr_info("%s: Going to sleep...\n", __func__);
			ret = wait_event_interruptible(dev->write_wq,
				ringbuffer_available_space(dev->rb) > 0);
			if (ret) {
				pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
				break;
			}
		}
		ret = ringbuffer_write(dev->rb, &buff[i], 1);
		if (ret != 1) {
			pr_err("%s: Failed to write to ringbuffer\n", __func__);
			break;
		}
		msleep(1000);	/* This is introduced to see the transfer */

		/* Awake any readers */
		wake_up_interruptible(&dev->read_wq);
	}

	kfree((void *)work);
}

/**
 * @page p7
 * @section p7s7 Step 7: Modify the exit function
 *
 * The `exit` funtion has to undo everything the `init` function did. In this
 * lab, the `init` function created the work queues for the devices (inside the
 * function `mydev_init_device`). Then the `exit` function has to destroy those
 * work queues by first calling `flush_workqueue` to ensure that any scheduled
 * work has run to completion, and then calling `destroy_workqueue`:
 *
 * @code
 * flush_workqueue(my_devices[i].workq);
 * destroy_workqueue(my_devices[i].workq);
 * @endcode
 */

/*
 * mydev_exit()
 * Module exit function. It is called when rmmod is executed.
 */
static void mydev_exit(void)
{
	int i;
	dev_t devid = MKDEV(major, first_minor);

	/* Deregister char devices from kernel and free memory*/
	if (my_devices) {
		for (i = 0; i < num_devices; i++) {
			pr_info("Flushing %s workqueue...\n", my_devices[i].name);
			flush_workqueue(my_devices[i].workq);
			destroy_workqueue(my_devices[i].workq);

			cdev_del(&my_devices[i].cdev);
			ringbuffer_destroy(my_devices[i].rb);

			if (my_devices[i].dev_mem) {
				pr_info("dev_mem%d = %s\n", first_minor+i,
						(char *)my_devices[i].dev_mem);
				free_page((unsigned long)my_devices[i].dev_mem);
			}
		}
		kfree(my_devices);
	}

	/* Release MAJOR and MINOR numbers */
	unregister_chrdev_region(devid, num_devices);

	/* Deregister device from sysfs */
	if (mydev_class != NULL) {
		for (i = 0; i < num_devices; i++)
			device_destroy(mydev_class, MKDEV(major, first_minor + i));
		class_destroy(mydev_class);
	}

	pr_info("Driver \"%s\" unloaded...\n", DEVICE_NAME);
}

/*
 * mydev_init()
 * Module init function. It is called when insmod is executed.
 */
static int __init mydev_init(void)
{
	int ret = 0;
	int i;
	dev_t devid;

	/* Dynamic allocation of MAJOR and MINOR numbers */
	ret = alloc_chrdev_region(&devid, first_minor, num_devices, DEVICE_NAME);
	if (ret) {
		pr_err("Error %d: Failed registering major number\n", ret);
		return ret;
	}

	/* save MAJOR number */
	major = MAJOR(devid);

	/* Allocate devices */
	my_devices = kzalloc(num_devices * sizeof(struct my_dev), GFP_KERNEL);
	if (!my_devices) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to allocate my_devices\n", ret);
		goto fail;
	}

	/* Initialize devices and add them to the system */
	for (i = 0; i < num_devices; i++) {
		ret = mydev_init_device(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;

		ret = mydev_setup_cdev(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;
	}

	/* Create a class and register with the sysfs. (Failure is not fatal) */
	mydev_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(mydev_class)) {
		pr_info("class_create() failed: %ld\n", PTR_ERR(mydev_class));
		mydev_class = NULL;
		ret = -EFAULT;
		goto fail;
	}
	/* Register device with sysfs (creates device node in /dev) */
	for (i = 0; i < num_devices; i++) {
		mydev_device = device_create(mydev_class, NULL,
						MKDEV(major, first_minor + i),
						NULL, DEVICE_NAME"%d", first_minor+i);
		if (IS_ERR(mydev_device)) {
			pr_info("device_create() failed: %ld\n",
							PTR_ERR(mydev_device));
			mydev_device = NULL;
		}
	}

	pr_info("Driver \"%s\" loaded...\n", DEVICE_NAME);
	return 0;

fail:
	mydev_exit();
	return ret;
}

module_init(mydev_init);
module_exit(mydev_exit);

MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("This Driver shows how to defer execution of a function "
							"by using work queues");

/**
 * @page p7
 * @section p7s8 Summary
 *
 * After following all the steps described, your source code should look similar
 * to this:
 *
 * @include time_mgmt.c
 */
