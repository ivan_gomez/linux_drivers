/**
 * @file time_mgmt.h - Deferred execution through work queues.
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 * @author Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * }
 */

#ifndef __CHAR_DRV_H
#define __CHAR_DRV_H

#include <linux/workqueue.h>
#include "ringbuffer.h"
#include "chardrv_ioctl.h"

#define KBUFF_SIZE	16	/* size of the kernel side buffer */
#define DEVICE_NAME	"mydev"

struct my_dev {
	char name[32];			/* device name */
	struct ringbuffer *rb;		/* Data buffer */
	struct cdev cdev;		/* cdev */
	void *dev_mem;			/* pointer to device memory */
	wait_queue_head_t read_wq;	/* read wait queue */
	wait_queue_head_t write_wq;	/* write wait queue */
	struct workqueue_struct *workq;	/* work queue */
};

struct my_work_t {
	struct work_struct my_work;
	struct data_xfer d_xfer;
	struct my_dev *dev;
};

/* Function prototypes */
int mydev_init_device(struct my_dev *dev, unsigned int minor);
int mydev_setup_cdev(struct my_dev *dev, unsigned int minor);
void mydev_workq_func(struct work_struct *work);

#endif /* __CHAR_DRV_H */
