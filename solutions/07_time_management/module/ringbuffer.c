/**
 * @file 07_time_management/module/ringbuffer.c
 * @brief Ring Buffer
 *
 * Authors: Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 *          Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ringbuffer.h"
#include <linux/slab.h>

/*
 * ringbuffer_create
 * Allocates and initializes a ringbuffer
 * size: The size of the ringbuffer
 * Returns a pointer to the ringbuffer or NULL if fails to create it
 */
struct ringbuffer *ringbuffer_create(unsigned int size)
{
	struct ringbuffer *rb;

	rb = kzalloc(sizeof(struct ringbuffer), GFP_KERNEL);
	if (!rb) {
		pr_info("Failed to allocate ringbuffer\n");
		return NULL;
	}

	rb->buff = kmalloc(size, GFP_KERNEL);
	if (!rb->buff) {
		pr_info("Failed to allocate ringbuffer->buff\n");
		kfree(rb);
		return NULL;
	}

	rb->size = size;

	return rb;
}

/*
 * ringbuffer_destroy
 * Destroy the ringbuffer
 * rb: Pointer to the ringbuffer to free
 */
void ringbuffer_destroy(struct ringbuffer *rb)
{
	if (rb)
		kfree(rb->buff);
	kfree(rb);
	rb = NULL;
}

/*
 * ringbuffer_available_data
 * Indicates the amount of data that is available in the ring buffer.
 * rb: pointer to the ringbuffer
 * Returns the number of bytes available or -EFAULT if invalid pointer.
 */
int ringbuffer_available_data(struct ringbuffer *rb)
{
	int available;

	if (!rb) {
		pr_err("Error: ringbuffer is NULL\n");
		return -EFAULT;
	}

	available = rb->count;
	pr_info("Available data: %d\n", available);

	return available;
}

/*
 * ringbuffer_available_space
 * Indicates the available free space in the ringbuffer
 * rb: Pointer to the ringbuffer
 * Returns the number of bytes of free space or -EFAULT if invalid pointer.
 */
int ringbuffer_available_space(struct ringbuffer *rb)
{
	int available;

	if (!rb) {
		pr_err("Error: ringbuffer is NULL\n");
		return -EFAULT;
	}

	available = rb->size - rb->count;
	pr_info("Available space: %d\n", available);

	return available;
}

/*
 * ringbuffer_read
 * Reads data from the ringbuffer.
 * rb: Pointer to the ringbuffer.
 * data: Buffer to copy the data to.
 * num: Number of bytes to read.
 * Returns the number of bytes copied, or a negative error code.
 */
int ringbuffer_read(struct ringbuffer *rb, char *data, unsigned int num)
{
	int available;
	int part1;
	int part2;

	if (!rb)
		return -EFAULT;

	pr_info("rb->start = %d,  rb->end = %d,  rb->count = %d\n",
						rb->start, rb->end, rb->count);

	available = ringbuffer_available_data(rb);
	if (available == 0) {
		pr_info("Buffer is empty\n");
		return 0;
	}

	if (num > available) {
		pr_info("Not enough data. Requested: %d, Available: %d",
							num, available);
		num = available;
	}

	part1 = rb->size - rb->start; /* Data available at the end of the buf */
	if (num > part1) {
		part2 = num - part1;
		pr_info("Writing part1: %d\n", part1);
		memcpy(data, &rb->buff[rb->start], part1);
		pr_info("Writing part2: %d\n", part2);
		memcpy(&data[part1], &rb->buff[0], part2);
		rb->start = part2;
	} else {
		memcpy(data, &rb->buff[rb->start], num);
		rb->start = (rb->start + num) % rb->size;
	}

	rb->count -= num;
	pr_info("rb->start = %d,  rb->end = %d,  rb->count = %d\n",
						rb->start, rb->end, rb->count);

	return num;
}

/*
 * ringbuffer_write
 * Writes data to the ring buffer.
 * rb: Pointer to the ringbuffer.
 * data: Buffer to copy the data from.
 * num: Number of bytes to write.
 * Returns the number of bytes written, or a negative error code.
 */
int ringbuffer_write(struct ringbuffer *rb, const char *data, unsigned int num)
{
	int available;
	int part1;
	int part2;

	if (!rb)
		return -EFAULT;

	pr_info("rb->start = %d,  rb->end = %d,  rb->count = %d\n",
						rb->start, rb->end, rb->count);

	available = ringbuffer_available_space(rb);
	if (available == 0) {
		pr_info("Buffer is full\n");
		return -ENOMEM;
	}

	if (num > available) {
		pr_info("Not enough space. Requested: %d, Available: %d",
							num, available);
		num = available;
	}

	part1 = rb->size - rb->end; /* Space available at the end of the buff */
	if (num > part1) {
		part2 = num - part1;
		pr_info("Writing part1: %d\n", part1);
		memcpy(&rb->buff[rb->end], data, part1);
		pr_info("Writing part2: %d\n", part2);
		memcpy(&rb->buff[0], &data[part1], part2);
		rb->end = part2;
	} else {
		memcpy(&rb->buff[rb->end], data, num);
		rb->end = (rb->end + num) % rb->size;
	}

	rb->count += num;
	pr_info("rb->start = %d,  rb->end = %d,  rb->count = %d\n",
						rb->start, rb->end, rb->count);

	return num;
}
