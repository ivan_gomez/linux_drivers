#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include "chardrv_ioctl.h"

#define DEVICE "/dev/mydev0"
#define BUFF_SIZE	1024

int write_to_device(char *dev_name, char *str, int num)
{
	int fd;
	int rc;
	int bytes_copied;

	fd = open(dev_name, O_RDWR);
	if (fd < 0) {
		printf("Error while opening the device\n");
		return fd;
	}
	printf("Opened: %s\n", dev_name);

	bytes_copied = write(fd, str, num);
	if (bytes_copied < 0) {
		printf("Error while writing to %s\n", dev_name);
		return bytes_copied;
	}
	printf("Wrote: %s\n", str);

	rc = close(fd);
	if (rc < 0) {
		printf("Error while closing %s\n", dev_name);
		return rc;
	}
	printf("Closed: %s\n", dev_name);

	return bytes_copied;
}

int read_from_device(char *dev_name, char *str, int num)
{
	int fd;
	int rc;
	int bytes_copied;

	fd = open(dev_name, O_RDWR);
	if (fd < 0) {
		printf("Error while opening the device\n");
		return fd;
	}
	printf("Opened: %s\n", dev_name);

	bytes_copied = read(fd, str, num);
	if (bytes_copied < 0) {
		printf("Error while reading from %s\n", dev_name);
		return bytes_copied;
	}
	printf("Read: %s\n", str);

	rc = close(fd);
	if (rc < 0) {
		printf("Error while closing the device\n");
		return rc;
	}
	printf("Closed: %s\n", dev_name);

	return bytes_copied;
}

int send_cmd_device(char *dev_name, unsigned int cmd, long arg)
{
	int fd;
	int rc;

	fd = open(dev_name, O_RDWR);
	if (fd < 0) {
		printf("Error while opening the device\n");
		return fd;
	}
	printf("Opened: %s\n", dev_name);

	rc = ioctl(fd, cmd, arg);
	if (rc) {
		printf("Error while sending IOCTL command to %s\n", dev_name);
		return rc;
	}
	printf("IOCTL sent successfully to %s\n", dev_name);

	rc = close(fd);
	if (rc < 0) {
		printf("Error while closing the device\n");
		return rc;
	}
	printf("Closed: %s\n", dev_name);

	return 0;

}


char *mmap_device(char *dev_name, unsigned int size)
{
	int fd;
	int rc;
	char *addr;

	fd = open(DEVICE, O_NONBLOCK | O_RDWR);
        if (fd < 0) {
                printf("Error while opening the device\n");
                return NULL;
        }
	printf("Opened: %s\n", dev_name);

        addr = (char *)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (addr == MAP_FAILED) {
                printf("Failed to map device memory\n");
                close(fd);
                return NULL;
        }
        printf("map address = %p\n", addr);

	rc = close(fd);
	if (rc < 0) {
		printf("Error while closing the device\n");
		return NULL;
	}
	printf("Closed: %s\n", dev_name);

	return addr;
}

void pr_help(char *prog_name)
{
	printf("Usage:\n");
	printf("%s <device_name> \"<string to write>\"\n", prog_name);
}

int main(int argc, char *argv[])
{
	int rc;
	char buff[BUFF_SIZE];

	char *device_name;
	char *data;
	char *map;
	struct data_xfer xfer;

	if (argc != 3) {
		pr_help(argv[0]);
		return -1;
	}

	device_name = argv[1];
	data = argv[2];

	rc = write_to_device(device_name, data, strlen(data) + 1);
	if (rc < 0) {
		printf("Failed to write to device\n");
		return -1;
	}

	rc = read_from_device(device_name, buff, BUFF_SIZE);
	if (rc < 0) {
		printf("Failed to read from device\n");
		return -1;
	}

	map = mmap_device(device_name, BUFF_SIZE);
	if (!map) {
		printf("Failed to mmap device memory\n");
		return -1;
	}

	map = strcpy(map, "abcdef");
	xfer.start = 0;
	xfer.count = 6;
	rc = send_cmd_device(device_name, MYDEV_START_XFER, (long)&xfer);

	map = strcat(map, "ABCDEF");
	xfer.start = 6;
	xfer.count = 6;
	rc = send_cmd_device(device_name, MYDEV_START_XFER, (long)&xfer);

	map = strcat(map, "123456");
	xfer.start = 12;
	xfer.count = 6;
	rc = send_cmd_device(device_name, MYDEV_START_XFER, (long)&xfer);


	return 0;
}
