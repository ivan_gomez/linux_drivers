/**
 * @file mmap_drv.c
 * @brief Character driver implementing memory mapping
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 * @author Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * }
 */

/**
 * @page p6 Lab 06: Memory mapping
 *
 * In this lab you will learn how to map kernel memory into user address space.
 *
 * This lab involves the creation of an application to request memory mapping
 * by using the `mmap` system call. After mapping, your application will be able
 * to read and/or write directly to the kernel memory by using the pointer
 * returned by the `mmap` function.
 *
 * Your driver will be responsible of two things:
 * 1. Allocate kernel memory to be mapped.
 * 2. Implement the `mmap` function of the `file_operations` structure,
 * similarly like you did with the `read` or `write` functions that you have
 * previously implemented.
 */

/**
 * @page p6
 * @section p6s1 Step 1: The module header file
 *
 * Edit the mmap_drv.h file and include a pointer to `void` in the structure
 * `my_dev`:
 * @code
 * void *dev_mem;
 * @endcode
 *
 * This pointer will contain the logical kernel address of the device memory.
 * This is the memory that you will map into the user address space. Each device
 * will have its own memory.
 */

/**
 * @page p6
 * @section p6s2 Step 2: Add the necessary header files
 *
 * Now edit the mmap_drv.c file. You will have to add the following header
 * files:
 *
 * @code
 * #include <linux/mm.h>
 * #include "mmap_drv.h"
 * @endcode
 *
 * The header file `linux/mm.h` contains the functions needed to map memory.
 * The header file `mmap_drv.h` is the header file you have just modified above.
 */

#include <linux/module.h>	/* Dynamic loading of modules into the kernel */
#include <linux/fs.h>		/* file defs of important data structures */
#include <linux/cdev.h>		/* char devices structure and aux functions */
#include <linux/device.h>	/* generic, centralized driver model */
#include <linux/uaccess.h>	/* for accessing user-space */
#include <linux/moduleparam.h>	/* Module parameters */
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/errno.h>	/* Error codes */
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/wait.h>		/* for wait_event and wake_up */
#include <linux/sched.h>	/* for TASK_INTERRUPTIBLE */
#include <linux/mm.h>		/* for memory mapping (mmap) */
#include "mmap_drv.h"

static unsigned major;			/* major number */
static unsigned first_minor;		/* first minor number */
static unsigned num_devices = 10;	/* number of devices */
static unsigned rb_size = KBUFF_SIZE;	/* ring buffer size */
static struct class *mydev_class;	/* to register with sysfs */
static struct device *mydev_device;	/* to register with sysfs */

struct my_dev *my_devices;	/* array of structs of num_devices elements */

/* Module parameters */
module_param(first_minor, uint, S_IRUGO);
module_param(num_devices, uint, S_IRUGO);
module_param(rb_size, uint, S_IRUGO);

MODULE_PARM_DESC(first_minor, "First minor number");
MODULE_PARM_DESC(num_devices, "Number of devices");
MODULE_PARM_DESC(rb_size, "Ring Buffer size");

/*
 * mydev_read()
 * This function implements the read function in the file operation structure.
 */
static ssize_t mydev_read(struct file *filp, char __user *buffer, size_t nbuf,
								loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (ringbuffer_available_data(dev->rb) == 0) {
		/* If there is nothing to read and O_NONBLOCK is set, return */
		if (filp->f_flags & O_NONBLOCK) {
			pr_info("O_NONBLOCK flag set\n");
			return -EAGAIN;
		}

		/* Go to sleep until data is available */
		/* If interrupted by a signal, return -ERESTARTSYS */
		pr_info("%s: Going to sleep...\n", __func__);
		ret = wait_event_interruptible(dev->read_wq,
					ringbuffer_available_data(dev->rb) > 0);
		if (ret) {
			pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
			return -ERESTARTSYS;
		}
	}

	ret = access_ok(VERIFY_WRITE, buffer, nbuf);
	if (!ret) {
		pr_err("Invalid user address: %p\n", (void *)buffer);
		ret = -EFAULT;
	} else {
		ret = ringbuffer_read(dev->rb, buffer, nbuf);
	}

	/* Awake any writers */
	wake_up_interruptible(&dev->write_wq);

	return ret;
}

/*
 * mydev_write()
 * This function implements the write function in the file operation structure.
 */
static ssize_t mydev_write(struct file *filp, const char __user *buffer,
						size_t nbuf, loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (ringbuffer_available_space(dev->rb) == 0) {
		/* If the ring buffer is full and O_NONBLOCK is set, return */
		if (filp->f_flags & O_NONBLOCK) {
			pr_info("O_NONBLOCK flag set\n");
			return -EAGAIN;
		}

		/* Go to sleep until there is space in the ring buffer */
		/* If interrupted by a signal, return -ERESTARTSYS */
		pr_info("%s: Going to sleep...\n", __func__);
		ret = wait_event_interruptible(dev->write_wq,
				ringbuffer_available_space(dev->rb) > 0);
		if (ret) {
			pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
			return -ERESTARTSYS;
		}
	}

	ret = access_ok(VERIFY_READ, buffer, nbuf);
	if (!ret) {
		pr_err("Invalid user address: %p\n", (void *) buffer);
		ret = -EFAULT;
	} else {
		ret = ringbuffer_write(dev->rb, buffer, nbuf);
		pr_info("Copied %d bytes\n", ret);
	}

	/* Awake any readers */
	wake_up_interruptible(&dev->read_wq);

	return ret;
}

/*
 * Function called when the user side application opens a handle to mydev driver
 * by calling the open() function.
 */

static int mydev_open(struct inode *ip, struct file *filp)
{
	struct my_dev *dev;

	pr_info("[%d] %s\n", __LINE__, __func__);
	/* Get my_dev structure for the device and assign it to private data*/
	dev = container_of(ip->i_cdev, struct my_dev, cdev);
	filp->private_data = dev;

	return 0;
}

/*
 * Function called when the user side application closes a handle to mydev
 * driver by calling the close() function.
 */

static int mydev_close(struct inode *ip, struct file *filp)
{
	pr_info("[%d] %s\n", __LINE__, __func__);

	return 0;
}

/**
 * @page p6
 * @section p6s3 Step 3: Implement the mmap function
 *
 * Write a function with the following prototype:
 * @code
 * static int mydev_mmap(struct file *filp, struct vm_area_struct *vma);
 * @endcode
 *
 * The parameter `filp` is a pointer to the `file` structure. It is the same as
 * the one used in the `read` and `write` functions.
 * The parameter `vma` is a pointer that contains information about the virtual
 * address range that is used to access the device.
 *
 * Inside this function you have to do the following:
 *
 * 1. Declare and initialize the variables needed for this fucntion:
 * @code
 * unsigned long pfn;
 * unsigned long start = (unsigned long)vma->vm_start;
 * unsigned long size = (unsigned long)(vma->vm_end - vma->vm_start);
 * pgprot_t prot = vma->vm_page_prot;
 * struct my_dev *dev = filp->private_data;
 * @endcode
 * The variable `pfn` will contain the page frame number, remember that `mmap`
 * maps pages, so you need to specify which page number you are going to map,
 * and not the physical address.
 * The variable `start` represents the virtual address where the remapping
 * should begin.
 * The variable `size` corresponds to the size, in bytes, of the
 * memory that is being mapped.
 * The `prot` variable is the requested protection for the virtual memory area.
 * And The variable `dev` is a pointer to the structure `my_dev`, which is
 * needed to have access to the device memory (`dev->dev_mem`) that is going to
 * be mapped. `dev_mem` should be already allocated by the `init` function.
 *
 * 2. In this lab we are going to map only one page. If the user application
 * requested a size different than a page, then return an error:
 * @code
 * if (size != PAGE_SIZE) {
 *	pr_err("Req size = %ld, device buffer size = %ld", size, PAGE_SIZE);
 *	return -EINVAL;
 * }
 * @endcode
 *
 * 3. Get the page frame number (`pfn`) from the device memory address
 * (`dev->dev_mem`). Remember that memory allocated with `kmalloc()` or
 * `get_free_pages()` are kernel logical addresses, so first you have to convert
 * it to a physical address (by using the `__pa` macro), and then get the page
 * frame number by shifting `PAGE_SHIFT` bits to the right. On systems with a
 * page size of 4 KB, the `PAGE_SHIFT` is 12.
 * @code
 * pfn = (__pa(dev->dev_mem)) >> PAGE_SHIFT;
 * @endcode
 *
 * 4. Use the `remap_pfn_range` function to remap the physical addresses. It
 * has following prototype:
 * @code
 * int remap_pfn_range(struct vm_area_struct *vma,
 *			unsigned long virt_addr,
 *			unsigned long pfn,
 *			unsigned long size,
 *			pgprot_t prot);
 * @endcode
 * If this function fails it will return a negative error code, otherwise it
 * will return 0.
 * 
 * 5. Check the returned value from `remap_pfn_range`, and in case of failure
 * return the error code to let the user application know that the mapping
 * failed.
 */

/*
 * mydev_mmap
 * This fuction maps defice memory into user virtual address space.
 */
static int mydev_mmap(struct file *filp, struct vm_area_struct *vma)
{
	unsigned long pfn;
	unsigned long start = (unsigned long)vma->vm_start;
	unsigned long size = (unsigned long)(vma->vm_end - vma->vm_start);
	struct my_dev *dev = filp->private_data;

	pr_info("mmap function was called\n");

	/* If size requested is different than the allocated memory*/
	if (size != PAGE_SIZE) {
		pr_err("Req size = %ld, device buffer size = %ld",
							size, PAGE_SIZE);
		return -EINVAL;
	}

	pfn = (__pa(dev->dev_mem)) >> PAGE_SHIFT;
	if (remap_pfn_range(vma, start, pfn, size, vma->vm_page_prot)) {
		pr_err("Failed to remap");
		return -EAGAIN;
	}

	pr_info("dev_mem: Logical = 0x%p, Physical = 0x%p, pfn = 0x%p",
			dev->dev_mem, (void *)__pa(dev->dev_mem), (void *)pfn);

	return 0;
}

/**
 * @page p6
 * @section p6s4 Step 4: Add the mmap function to the file_operations structure
 *
 * To be able to handle the `mmap` system call, you have to add the function
 * `mydev_mmap` you have just created in the previous step, to the structure
 * `file_operations`, which now will look like:
 * @code
 * static const struct file_operations mydev_fops = {
 *	.open = mydev_open,
 *	.release = mydev_close,
 *	.owner = THIS_MODULE,
 *	.read = mydev_read,
 *	.write = mydev_write,
 *	.mmap = mydev_mmap
 * };
 * @endcode
 */

/*
 * File operation structure
 */
static const struct file_operations mydev_fops = {
	.open = mydev_open,
	.release = mydev_close,
	.owner = THIS_MODULE,
	.read = mydev_read,
	.write = mydev_write,
	.mmap = mydev_mmap
};

/**
 * @page p6
 * @section p6s5 Step 5: Allocate memory at driver initialization
 *
 * Inside the function `mydev_init_device`, you have to allocate the memory for
 * the device. This is the memory that is going to be mapped to the user space.
 * Since `mmap` only maps pages, the memory has to be aligned to a page, then
 * you have to use `__get_free_page` or `__get_free_pages` instead of `kmalloc`.
 * If it fails to allocate memory, return `-ENOMEM`.
 * You can print the logical and physical addresses if you want:
 * @code
 * dev->dev_mem = (void *)__get_free_page(GFP_KERNEL);
 * if (!dev->dev_mem) {
 * 	pr_err("Failed to allocate device memory\n");
 * 	ret = -ENOMEM;
 * 	return ret;
 * }
 * pr_info("dev_mem%d Addresses: Logical = 0x%p, Physical = 0x%p\n",
 * 		minor, dev->dev_mem, (void *)__pa(dev->dev_mem));
 * @endcode
 */

/*
 * mydev_init_device()
 * This function allocates memory and initilizes wait queues.
 * returns 0 on success, and a negative error code on failure.
 */
int mydev_init_device(struct my_dev *dev, unsigned int minor)
{
	int ret;

	if (sprintf(dev->name, DEVICE_NAME"%d", minor) < 0) {
		pr_err("Failed to set the device name\n");
		ret = -EINVAL;
		return ret;
	}

	dev->rb = ringbuffer_create(rb_size);
	if (!dev->rb) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to create ring buffer\n", ret);
		return ret;
	}

	dev->dev_mem = (void *)__get_free_page(GFP_KERNEL);
	if (!dev->dev_mem) {
		pr_err("Failed to allocate device memory\n");
		ret = -ENOMEM;
		return ret;
	}
	pr_info("dev_mem%d Addresses: Logical = 0x%p, Physical = 0x%p\n",
			minor, dev->dev_mem, (void *)__pa(dev->dev_mem));

	init_waitqueue_head(&dev->read_wq);
	init_waitqueue_head(&dev->write_wq);

	pr_info("device%d initializated successfully\n", minor);

	return 0;
}

/*
 * mydev_setup_cdev()
 * This function initializes and adds cdev to the system.
 */
int mydev_setup_cdev(struct my_dev *dev, unsigned int minor)
{
	int err = 0;
	dev_t devid = MKDEV(major, minor);

	cdev_init(&dev->cdev, &mydev_fops);
	dev->cdev.owner = THIS_MODULE;
	err = cdev_add(&dev->cdev, devid, 1);
	if (err)
		pr_err("Error %d adding %s%d", err, DEVICE_NAME, minor);

	return err;
}

/**
 * @page p6
 * @section p6s6 Step 6: Free memory at driver exit
 *
 * Inside the function `mydev_exit`, you have to free the memory that was
 * allocated when the module was inserted into the kernel.
 *
 * At driver initialization, you allocated one page for each device. Now you
 * have to free those pages by using the function `free_page`. You can do it
 * inside the `for` loop, after calling `ringbuffer_destroy`. This is also
 * a good place to print the content of that memory to see what information the
 * user application wrote to the mapped area.
 * @code
 * if (my_devices[i].dev_mem) {
 *	pr_info("dev_mem%d = %s\n", first_minor+i, (char *)my_devices[i].dev_mem);
 *	free_page((unsigned long)my_devices[i].dev_mem);
 * }
 * @endcode
 */

/*
 * mydev_exit()
 * Module exit function. It is called when rmmod is executed.
 */
static void mydev_exit(void)
{
	int i;
	dev_t devid = MKDEV(major, first_minor);

	/* Deregister char devices from kernel and free memory*/
	if (my_devices) {
		for (i = 0; i < num_devices; i++) {
			cdev_del(&my_devices[i].cdev);
			ringbuffer_destroy(my_devices[i].rb);
			if (my_devices[i].dev_mem) {
				pr_info("dev_mem%d = %s\n", first_minor+i,
						(char *)my_devices[i].dev_mem);
				free_page((unsigned long)my_devices[i].dev_mem);
			}
		}
		kfree(my_devices);
	}

	/* Release MAJOR and MINOR numbers */
	unregister_chrdev_region(devid, num_devices);

	/* Deregister device from sysfs */
	if (mydev_class != NULL) {
		for (i = 0; i < num_devices; i++)
			device_destroy(mydev_class, MKDEV(major, first_minor + i));
		class_destroy(mydev_class);
	}

	pr_info("Driver \"%s\" unloaded...\n", DEVICE_NAME);
}

/*
 * mydev_init()
 * Module init function. It is called when insmod is executed.
 */
static int __init mydev_init(void)
{
	int ret = 0;
	int i;
	dev_t devid;

	/* Dynamic allocation of MAJOR and MINOR numbers */
	ret = alloc_chrdev_region(&devid, first_minor, num_devices, DEVICE_NAME);
	if (ret) {
		pr_err("Error %d: Failed registering major number\n", ret);
		return ret;
	}

	/* save MAJOR number */
	major = MAJOR(devid);

	/* Allocate devices */
	my_devices = kzalloc(num_devices * sizeof(struct my_dev), GFP_KERNEL);
	if (!my_devices) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to allocate my_devices\n", ret);
		goto fail;
	}

	/* Initialize devices and add them to the system */
	for (i = 0; i < num_devices; i++) {
		ret = mydev_init_device(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;

		ret = mydev_setup_cdev(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;
	}

	/* Create a class and register with the sysfs. (Failure is not fatal) */
	mydev_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(mydev_class)) {
		pr_info("class_create() failed: %ld\n", PTR_ERR(mydev_class));
		mydev_class = NULL;
		ret = -EFAULT;
		goto fail;
	}
	/* Register device with sysfs (creates device node in /dev) */
	for (i = 0; i < num_devices; i++) {
		mydev_device = device_create(mydev_class, NULL,
						MKDEV(major, first_minor + i),
						NULL, my_devices[i].name);
		if (IS_ERR(mydev_device)) {
			pr_info("device_create() failed: %ld\n",
							PTR_ERR(mydev_device));
			mydev_device = NULL;
		}
	}

	pr_info("Driver \"%s\" loaded...\n", DEVICE_NAME);
	return 0;

fail:
	mydev_exit();
	return ret;
}

module_init(mydev_init);
module_exit(mydev_exit);

MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("This Char Driver shows how to map kernel-space memory "
		"to user-space");

/**
 * @page p6
 * @section p6s7 Summary
 *
 * After following all the steps described, your source code should look similar
 * to this:
 *
 * @include mmap_drv.c
 */
