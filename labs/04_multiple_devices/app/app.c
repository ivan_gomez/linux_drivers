#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define BUFF_SIZE	1024

/* Write to devices */
int write_to_device(char *dev_name, char *str, int len)
{
	int fd;
	int rc;
	int bytes_copied;

	fd = open(dev_name, O_RDWR);
	if (fd < 0) {
		printf("Error while opening the device\n");
		return fd;
	}
	printf("Opened: %s\n", dev_name);
	bytes_copied = write(fd, str, len);
	if (bytes_copied < 0) {
		printf("Error while writing to %s\n", dev_name);
		return bytes_copied;
	}
	printf("Wrote: %s\n", str);

	rc = close(fd);
	if (rc < 0) {
		printf("Error while closing %s\n", dev_name);
		return rc;
	}
	printf("Closed: %s\n", dev_name);

	return bytes_copied;
}

/* Read from to devices */
int read_from_device(char *dev_name, char *str, int len)
{
	int fd;
	int rc;
	int bytes_copied;

	fd = open(dev_name, O_RDWR);
	if (fd < 0) {
		printf("Error while opening the device\n");
		return fd;
	}
	printf("Opened: %s\n", dev_name);

	bytes_copied = read(fd, str, len);
	if (bytes_copied < 0) {
		printf("Error while reading from %s\n", dev_name);
		return bytes_copied;
	}
	printf("Read: %s\n", str);

	rc = close(fd);
	if (rc < 0) {
		printf("Error while closing the device\n");
		return rc;
	}
	printf("Closed: %s\n", dev_name);

	return bytes_copied;
}

void pr_help(char *prog_name)
{
	printf("Usage:\n");
	printf("%s <device_name> \"<string to write>\"\n .....", prog_name);
}

int main(int argc, char *argv[])
{
	int i;
	int rc;
	int num_devices;
	char buff[BUFF_SIZE];

	if(argc < 3)
	{
		pr_help(argv[0]);
		return -1;
	}

	num_devices = (argc - 1)/2;
	char *device_name[num_devices];
	char *data[num_devices];

	for(i=0 ; i < num_devices ; i++)
	{
		device_name[i] = argv[(i*2)+1];
		data[i] = argv[(i*2)+2];
	
		rc = write_to_device(device_name[i], data[i], strlen(data[i]) + 1);
		if (rc < 0) {
			printf("Failed to write to device\n");
			return -1;
		}

		rc = read_from_device(device_name[i], buff, BUFF_SIZE);
		if (rc < 0) {
			printf("Failed to read from device\n");
			return -1;
		}
	}

	return 0;
}
