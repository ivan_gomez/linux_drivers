/* This app shows how to use mmap */

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>


#define DEVICE "/dev/mydev0"
#define BUFF_SIZE	10

int main()
{
	int fd;
	int rc;
	char * map;
	void *ptr = NULL;

	fd = open(DEVICE, O_NONBLOCK | O_RDWR);
	if (fd < 0) {
		printf("Error while opening the device\n");
		return fd;
	}
	printf("Open\n");

	map = (char *)mmap(ptr, 16, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (map == MAP_FAILED) {
		printf("Failed to map device memory\n");
		close(fd);
		return -1;
	}
	printf("map address = %p\n", map);

	strcpy(map, "hola");
	printf("map = %s\n", map);

	if (munmap(map, 16)) {
		printf("Failed to unmap\n");
	}

	/* This will cause a segmentation fault because memory is unmapped
	 * Check dmesg and the new string "hola2" should not be there. */
	memset(map, 0, 16);
	strcpy(map, "hola2");
	printf("map = %s\n", map);
	printf("map address = %p\n", map);

	rc = close(fd);

	return rc;
}
