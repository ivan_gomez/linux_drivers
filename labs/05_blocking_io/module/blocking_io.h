/**
 * mult_dev.h - Character driver for multiple devices
 *
 * Authors: Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 *          Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CHAR_DRV_H
#define __CHAR_DRV_H

#define KBUFF_MAX_SIZE	1024	/* size of the kernel side buffer */
#define DEVICE_NAME	"mydev"

struct my_dev {
	char *kbuf;		/* Data buffer */
	unsigned int kbuf_size;	/* Max buffer size */
	unsigned int kbuf_len;	/* Number of data bytes in kbuf */
	struct cdev cdev;	/* cdev */
};

/* Function prototypes */
int mydev_setup_cdev(struct my_dev *dev, unsigned int minor);

#endif /* __CHAR_DRV_H */
