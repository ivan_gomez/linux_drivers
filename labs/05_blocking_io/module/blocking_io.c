/**
 * mult_dev.c - Character driver for multiple devices
 *
 * Authors: Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 *          Adrian Ortega Garcia <adrianog.sw@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>	/* Dynamic loading of modules into the kernel */
#include <linux/fs.h>		/* file defs of important data structures */
#include <linux/cdev.h>		/* char devices structure and aux functions */
#include <linux/device.h>	/* generic, centralized driver model */
#include <linux/uaccess.h>	/* for accessing user-space */
#include <linux/moduleparam.h>	/* Module parameters */
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/errno.h>	/* Error codes */
#include <linux/fcntl.h>	/* O_ACCMODE */
#include "mult_dev.h"

static unsigned major;			/* major number */
static unsigned first_minor;		/* first minor number */
static unsigned num_devices = 10;	/* number of devices */
static struct class *mydev_class;	/* to register with sysfs */
static struct device *mydev_device;	/* to register with sysfs */

struct my_dev *my_devices;	/* array of structs of num_devices elements */

/* Module parameters */
module_param(first_minor, uint, S_IRUGO);
module_param(num_devices, uint, S_IRUGO);

MODULE_PARM_DESC(first_minor, "First minor number");
MODULE_PARM_DESC(num_devices, "Number of devices");

/*
 * mydev_read()
 * This function implements the read function in the file operation structure.
 */
static ssize_t mydev_read(struct file *filp, char __user *buffer, size_t nbuf,
								loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;
	unsigned int kbuf_len = dev->kbuf_len;

	pr_info("[%d] %s\n", __LINE__, __func__);

	/* If offset is greater than kbuffer length, there is nothing to copy */
	if (*offset >= kbuf_len) {
		pr_info("Nothing to read: Offset = %d, kbuf_len = %d\n",
						(int)*offset, kbuf_len);
		goto out;
	}

	/* Check for maximum size of data available in kernel buffer */
	if ((nbuf + *offset) > kbuf_len)
		nbuf = kbuf_len - *offset;

	/* fill the buffer, return the number of bytes copied */
	ret = copy_to_user(buffer, &dev->kbuf[*offset], nbuf);
	if (ret) {
		pr_err("[%d] %s copy_to_user failed: %d\n",
						__LINE__, __func__, ret);
		ret = -EFAULT;
		goto out;
	}

	*offset += nbuf;
	ret = nbuf;

out:
	return ret;
}

/*
 * mydev_write()
 * This function implements the write function in the file operation structure.
 */
static ssize_t mydev_write(struct file *filp, const char __user *buffer,
						size_t nbuf, loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;
	unsigned int kbuf_len = dev->kbuf_len;
	unsigned int kbuf_size = dev->kbuf_size;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (*offset >= kbuf_size) {
		pr_info("No space to write: Offset = %d, kbuf_len = %d\n",
						(int)*offset, kbuf_len);
		goto out;
	}

	/* Check for maximum size of kernel buffer */
	if ((nbuf + *offset) > kbuf_size)
		nbuf = kbuf_size - *offset;

	/* fill the buffer, return the number of byres written */
	ret = copy_from_user(&dev->kbuf[*offset], buffer, nbuf);
	if (ret) {
		pr_err("[%d] %s copy_from_user failed: %d\n",
						__LINE__, __func__, ret);
		ret = -EFAULT;
		goto out;
	}

	*offset += nbuf;
	dev->kbuf_len = *offset;
	ret = nbuf;

out:
	return ret;
}

/*
 * Function called when the user side application opens a handle to mydev driver
 * by calling the open() function.
 */

static int mydev_open(struct inode *ip, struct file *filp)
{
	struct my_dev *dev;

	pr_info("[%d] %s\n", __LINE__, __func__);
	/* Get my_dev structure for the device and assign it to private data*/
	dev = container_of(ip->i_cdev, struct my_dev, cdev);
	filp->private_data = dev;

	return 0;
}

/*
 * Function called when the user side application closes a handle to mydev
 * driver by calling the close() function.
 */

static int mydev_close(struct inode *ip, struct file *filp)
{
	pr_info("[%d] %s\n", __LINE__, __func__);

	return 0;
}


/*
 * File operation structure
 */
static const struct file_operations mydev_fops = {
	.open = mydev_open,
	.release = mydev_close,
	.owner = THIS_MODULE,
	.read = mydev_read,
	.write = mydev_write
};

/*
 * mydev_setup_cdev()
 * This function initializes and adds cdev to the system.
 */
int mydev_setup_cdev(struct my_dev *dev, unsigned int minor)
{
	int err = 0;
	dev_t devid = MKDEV(major, minor);

	cdev_init(&dev->cdev, &mydev_fops);
	dev->cdev.owner = THIS_MODULE;
	err = cdev_add(&dev->cdev, devid, 1);
	if (err)
		pr_err("Error %d adding %s%d", err, DEVICE_NAME, minor);

	return err;
}

/*
 * mydev_exit()
 * Module exit function. It is called when rmmod is executed.
 */
static void mydev_exit(void)
{
	int i;
	dev_t devid = MKDEV(major, first_minor);

	/* Deregister char devices from kernel and free memory*/
	if (my_devices) {
		for (i = 0; i < num_devices; i++) {
			cdev_del(&my_devices[i].cdev);
			kfree(my_devices[i].kbuf);
		}
		kfree(my_devices);
	}

	/* Release MAJOR and MINOR numbers */
	unregister_chrdev_region(devid, num_devices);

	/* Deregister device from sysfs */
	if (mydev_class != NULL) {
		for (i = 0; i < num_devices; i++)
			device_destroy(mydev_class, MKDEV(major, first_minor + i));
		class_destroy(mydev_class);
	}

	pr_info("Driver \"%s\" unloaded...\n", DEVICE_NAME);
}

/*
 * mydev__init()
 * Module init function. It is called when insmod is executed.
 */
static int __init mydev_init(void)
{
	int ret = 0;
	int i;
	dev_t devid;

	/* Dynamic allocation of MAJOR and MINOR numbers */
	ret = alloc_chrdev_region(&devid, first_minor, num_devices, DEVICE_NAME);
	if (ret) {
		pr_err("Error %d: Failed registering major number\n", ret);
		return ret;
	}

	/* save MAJOR number */
	major = MAJOR(devid);

	/* Allocate devices */
	my_devices = kzalloc(num_devices * sizeof(struct my_dev), GFP_KERNEL);
	if (!my_devices) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to allocate my_devices\n", ret);
		goto fail;
	}

	/* Initialize devices and add them to the system */
	for (i = 0; i < num_devices; i++) {
		my_devices[i].kbuf = kmalloc(KBUFF_MAX_SIZE, GFP_KERNEL);
		if (!my_devices[i].kbuf) {
			ret = -ENOMEM;
			pr_err("Error %d: Failed to allocate kbuff\n", ret);
			goto fail;
		}
		my_devices[i].kbuf_size = KBUFF_MAX_SIZE;
		ret = mydev_setup_cdev(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;
	}

	/* Create a class and register with the sysfs. (Failure is not fatal) */
	mydev_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(mydev_class)) {
		pr_info("class_create() failed: %ld\n", PTR_ERR(mydev_class));
		mydev_class = NULL;
		ret = -EFAULT;
		goto fail;
	}
	/* Register device with sysfs (creates device node in /dev) */
	for (i = 0; i < num_devices; i++) {
		mydev_device = device_create(mydev_class, NULL,
						MKDEV(major, first_minor + i),
						NULL, DEVICE_NAME"%d", first_minor+i);
		if (IS_ERR(mydev_device)) {
			pr_info("device_create() failed: %ld\n",
							PTR_ERR(mydev_device));
			mydev_device = NULL;
		}
	}

	pr_info("Driver \"%s\" loaded...\n", DEVICE_NAME);
	return 0;

fail:
	mydev_exit();
	return ret;
}

module_init(mydev_init);
module_exit(mydev_exit);

MODULE_AUTHOR("Your Name Here <your.email@here.com>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("This Char Driver demostrates the use of Blocking IO");
