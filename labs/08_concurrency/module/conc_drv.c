/**
 * @file time_mgmt.c
 * @brief Deferred execution through work queues.
 *
 * @author Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>
 *
 * @license{
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * }
 */

#include <linux/module.h>	/* Dynamic loading of modules into the kernel */
#include <linux/fs.h>		/* file defs of important data structures */
#include <linux/cdev.h>		/* char devices structure and aux functions */
#include <linux/device.h>	/* generic, centralized driver model */
#include <linux/uaccess.h>	/* for accessing user-space */
#include <linux/moduleparam.h>	/* Module parameters */
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/errno.h>	/* Error codes */
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/wait.h>		/* for wait_event and wake_up */
#include <linux/sched.h>	/* for TASK_INTERRUPTIBLE */
#include <linux/mm.h>		/* for memory mapping (mmap) */
#include <linux/delay.h>	/* for msleep */
#include "chardrv_ioctl.h"	/* IOCTL commands */
#include "conc_drv.h"

static unsigned major;			/* major number */
static unsigned first_minor;		/* first minor number */
static unsigned num_devices = 10;	/* number of devices */
static unsigned rb_size = KBUFF_SIZE;	/* ring buffer size */
static struct class *mydev_class;	/* to register with sysfs */
static struct device *mydev_device;	/* to register with sysfs */

struct my_dev *my_devices;	/* array of structs of num_devices elements */

/* Module parameters */
module_param(first_minor, uint, S_IRUGO);
module_param(num_devices, uint, S_IRUGO);
module_param(rb_size, uint, S_IRUGO);

MODULE_PARM_DESC(first_minor, "First minor number");
MODULE_PARM_DESC(num_devices, "Number of devices");
MODULE_PARM_DESC(rb_size, "Ring Buffer size");

/*
 * mydev_read()
 * This function implements the read function in the file operation structure.
 */
static ssize_t mydev_read(struct file *filp, char __user *buffer, size_t nbuf,
								loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (ringbuffer_available_data(dev->rb) == 0) {
		/* If there is nothing to read and O_NONBLOCK is set, return */
		if (filp->f_flags & O_NONBLOCK) {
			pr_info("O_NONBLOCK flag set\n");
			return -EAGAIN;
		}

		/* Go to sleep until data is available */
		/* If interrupted by a signal, return -ERESTARTSYS */
		pr_info("%s: Going to sleep...\n", __func__);
		ret = wait_event_interruptible(dev->read_wq,
					ringbuffer_available_data(dev->rb) > 0);
		if (ret) {
			pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
			return -ERESTARTSYS;
		}
	}

	ret = access_ok(VERIFY_WRITE, buffer, nbuf);
	if (!ret) {
		pr_err("Invalid user address: %p\n", (void *)buffer);
		ret = -EFAULT;
	} else {
		ret = ringbuffer_read(dev->rb, buffer, nbuf);
	}

	/* Awake any writers */
	wake_up_interruptible(&dev->write_wq);

	return ret;
}

/*
 * mydev_write()
 * This function implements the write function in the file operation structure.
 */
static ssize_t mydev_write(struct file *filp, const char __user *buffer,
						size_t nbuf, loff_t *offset)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;

	pr_info("[%d] %s\n", __LINE__, __func__);

	if (ringbuffer_available_space(dev->rb) == 0) {
		/* If the ring buffer is full and O_NONBLOCK is set, return */
		if (filp->f_flags & O_NONBLOCK) {
			pr_info("O_NONBLOCK flag set\n");
			return -EAGAIN;
		}

		/* Go to sleep until there is space in the ring buffer */
		/* If interrupted by a signal, return -ERESTARTSYS */
		pr_info("%s: Going to sleep...\n", __func__);
		ret = wait_event_interruptible(dev->write_wq,
				ringbuffer_available_space(dev->rb) > 0);
		if (ret) {
			pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
			return -ERESTARTSYS;
		}
	}

	ret = access_ok(VERIFY_READ, buffer, nbuf);
	if (!ret) {
		pr_err("Invalid user address: %p\n", (void *) buffer);
		ret = -EFAULT;
	} else {
		ret = ringbuffer_write(dev->rb, buffer, nbuf);
		pr_info("Copied %d bytes\n", ret);
	}

	/* Awake any readers */
	wake_up_interruptible(&dev->read_wq);

	return ret;
}

/*
 * Function called when the user side application opens a handle to mydev driver
 * by calling the open() function.
 */

static int mydev_open(struct inode *ip, struct file *filp)
{
	struct my_dev *dev;

	pr_info("[%d] %s\n", __LINE__, __func__);
	/* Get my_dev structure for the device and assign it to private data*/
	dev = container_of(ip->i_cdev, struct my_dev, cdev);
	filp->private_data = dev;

	return 0;
}

/*
 * Function called when the user side application closes a handle to mydev
 * driver by calling the close() function.
 */

static int mydev_close(struct inode *ip, struct file *filp)
{
	pr_info("[%d] %s\n", __LINE__, __func__);

	return 0;
}

/*
 * mydev_mmap
 * This fuction maps defice memory into user virtual address space.
 */
static int mydev_mmap(struct file *filp, struct vm_area_struct *vma)
{
	unsigned long pfn;
	unsigned long start = (unsigned long)vma->vm_start;
	unsigned long size = (unsigned long)(vma->vm_end - vma->vm_start);
	struct my_dev *dev = filp->private_data;

	pr_info("mmap function was called\n");

	/* If size requested is different than the allocated memory*/
	if (size != PAGE_SIZE) {
		pr_err("Req size = %ld, device buffer size = %ld",
							size, PAGE_SIZE);
		return -EINVAL;
	}

	pfn = (__pa(dev->dev_mem)) >> PAGE_SHIFT;
	if (remap_pfn_range(vma, start, pfn, size, vma->vm_page_prot)) {
		pr_err("Failed to remap");
		return -EAGAIN;
	}

	pr_info("dev_mem: Logical = 0x%p, Physical = 0x%p, pfn = 0x%p",
			dev->dev_mem, (void *)__pa(dev->dev_mem), (void *)pfn);

	return 0;
}

/*
 * mydev_ioctl()
 * This function implements the ioctl function in the file operation structure.
 */
static long mydev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int ret = 0;
	struct my_dev *dev = filp->private_data;
	struct my_work_t *work;
	struct data_xfer *xfer = (struct data_xfer *) arg;

	switch(cmd) {
	case MYDEV_START_XFER:	/* Start data transfer */
		work = kmalloc(sizeof(struct my_work_t), GFP_KERNEL);
		if (work) {
			INIT_WORK((struct work_struct *)work, mydev_workq_func);
			work->d_xfer.start = xfer->start;
			work->d_xfer.count = xfer->count;
			work->dev = dev;
			ret = queue_work(dev->workq, (struct work_struct *)work);
			if (!ret) {
				pr_info("Already on the queue\n");
			} else {
				pr_info("Work submitted sucessfully to queue\n");
				ret = 0;	/* Change ret to 0 (success) */
			}
		} else {
			ret = -ENOMEM;
		}
		break;
	case MYDEV_STATUS_XFER:	/* Check the transfer status */
		break;
	case MYDEV_CANCEL_XFER:	/* Cancel data transfer */
		break;
	default:
		return -ENOTTY;	/* Inappropriate ioctl for device */
	}

	return ret;
}

/*
 * File operation structure
 */
static const struct file_operations mydev_fops = {
	.open = mydev_open,
	.release = mydev_close,
	.owner = THIS_MODULE,
	.read = mydev_read,
	.write = mydev_write,
	.mmap = mydev_mmap,
	.unlocked_ioctl = mydev_ioctl,
};

/*
 * mydev_init_device()
 * This function allocates memory and initilizes wait queues.
 * returns 0 on success, and a negative error code on failure.
 */
int mydev_init_device(struct my_dev *dev, unsigned int minor)
{
	int ret;

	if (sprintf(dev->name, DEVICE_NAME"%d", minor) < 0) {
		pr_err("Failed to set the device name\n");
		ret = -EINVAL;
		return ret;
	}

	dev->rb = ringbuffer_create(rb_size);
	if (!dev->rb) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to create ring buffer\n", ret);
		return ret;
	}

	dev->dev_mem = (void *)__get_free_page(GFP_KERNEL);
	if (!dev->dev_mem) {
		pr_err("Failed to allocate device memory\n");
		ret = -ENOMEM;
		return ret;
	}
	pr_info("dev_mem%d address = %p\n", minor, dev->dev_mem);

	init_waitqueue_head(&dev->read_wq);
	init_waitqueue_head(&dev->write_wq);

	dev->workq = create_workqueue(dev->name);
	if (!dev->workq) {
		pr_err("Failed to create work queue\n");
		ret = -ENOMEM;
		return ret;
	}

	return 0;
}

/*
 * mydev_setup_cdev()
 * This function initializes and adds cdev to the system.
 */
int mydev_setup_cdev(struct my_dev *dev, unsigned int minor)
{
	int err = 0;
	dev_t devid = MKDEV(major, minor);

	cdev_init(&dev->cdev, &mydev_fops);
	dev->cdev.owner = THIS_MODULE;
	err = cdev_add(&dev->cdev, devid, 1);
	if (err)
		pr_err("Error %d adding %s%d", err, DEVICE_NAME, minor);

	return err;
}

/*
 * mydev_workq_func
 * This is the function to be executed in the work queue.
 */
void mydev_workq_func(struct work_struct *work)
{
	int i;
	int ret = 0;
	struct my_work_t *my_work = (struct my_work_t *) work;
	struct my_dev *dev = my_work->dev;
	int start = my_work->d_xfer.start;
	int count = my_work->d_xfer.count;
	char *buff = dev->dev_mem;

	pr_info("%s: Work queue function was called\n", __func__);
	pr_info("start = %d, count = %d\n", start, count);

	for (i = start; i < start + count; i++) {
		if (ringbuffer_available_space(dev->rb) == 0) {
			pr_info("%s: Going to sleep...\n", __func__);
			ret = wait_event_interruptible(dev->write_wq,
				ringbuffer_available_space(dev->rb) > 0);
			if (ret) {
				pr_warning("%s: Error(%d): Interrupted by a signal\n",
								__func__, ret);
				break;
			}
		}
		ret = ringbuffer_write(dev->rb, &buff[i], 1);
		if (ret != 1) {
			pr_err("%s: Failed to write to ringbuffer\n", __func__);
			break;
		}
		msleep(1000);	/* This is introduced to see the transfer */

		/* Awake any readers */
		wake_up_interruptible(&dev->read_wq);
	}

	kfree((void *)work);
}

/*
 * mydev_exit()
 * Module exit function. It is called when rmmod is executed.
 */
static void mydev_exit(void)
{
	int i;
	dev_t devid = MKDEV(major, first_minor);

	/* Deregister char devices from kernel and free memory*/
	if (my_devices) {
		for (i = 0; i < num_devices; i++) {
			cdev_del(&my_devices[i].cdev);
			ringbuffer_destroy(my_devices[i].rb);

			if (my_devices[i].dev_mem) {
				pr_info("dev_mem%d = %s\n", first_minor+i,
						(char *)my_devices[i].dev_mem);
				free_page((unsigned long)my_devices[i].dev_mem);
			}

			flush_workqueue(my_devices[i].workq);
			destroy_workqueue(my_devices[i].workq);
		}
		kfree(my_devices);
	}

	/* Release MAJOR and MINOR numbers */
	unregister_chrdev_region(devid, num_devices);

	/* Deregister device from sysfs */
	if (mydev_class != NULL) {
		for (i = 0; i < num_devices; i++)
			device_destroy(mydev_class, MKDEV(major, first_minor + i));
		class_destroy(mydev_class);
	}

	pr_info("Driver \"%s\" unloaded...\n", DEVICE_NAME);
}

/*
 * mydev_init()
 * Module init function. It is called when insmod is executed.
 */
static int __init mydev_init(void)
{
	int ret = 0;
	int i;
	dev_t devid;

	/* Dynamic allocation of MAJOR and MINOR numbers */
	ret = alloc_chrdev_region(&devid, first_minor, num_devices, DEVICE_NAME);
	if (ret) {
		pr_err("Error %d: Failed registering major number\n", ret);
		return ret;
	}

	/* save MAJOR number */
	major = MAJOR(devid);

	/* Allocate devices */
	my_devices = kzalloc(num_devices * sizeof(struct my_dev), GFP_KERNEL);
	if (!my_devices) {
		ret = -ENOMEM;
		pr_err("Error %d: Failed to allocate my_devices\n", ret);
		goto fail;
	}

	/* Initialize devices and add them to the system */
	for (i = 0; i < num_devices; i++) {
		ret = mydev_init_device(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;

		ret = mydev_setup_cdev(&my_devices[i], first_minor + i);
		if (ret)
			goto fail;
	}

	/* Create a class and register with the sysfs. (Failure is not fatal) */
	mydev_class = class_create(THIS_MODULE, DEVICE_NAME);
	if (IS_ERR(mydev_class)) {
		pr_info("class_create() failed: %ld\n", PTR_ERR(mydev_class));
		mydev_class = NULL;
		ret = -EFAULT;
		goto fail;
	}
	/* Register device with sysfs (creates device node in /dev) */
	for (i = 0; i < num_devices; i++) {
		mydev_device = device_create(mydev_class, NULL,
						MKDEV(major, first_minor + i),
						NULL, DEVICE_NAME"%d", first_minor+i);
		if (IS_ERR(mydev_device)) {
			pr_info("device_create() failed: %ld\n",
							PTR_ERR(mydev_device));
			mydev_device = NULL;
		}
	}

	pr_info("Driver \"%s\" loaded...\n", DEVICE_NAME);
	return 0;

fail:
	mydev_exit();
	return ret;
}

module_init(mydev_init);
module_exit(mydev_exit);

MODULE_AUTHOR("Ivan Gomez Castellanos <ivan.gomez.castellanos@gmail.com>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("This Driver shows how to defer execution of a function "
							"by using work queues");
